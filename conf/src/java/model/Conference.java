package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Conference implements Conf{
    private String conf_id;
    private String conf_name;
    private String start_date;
    private String end_date;
    private String department;
    private String company;
    private String desc;
    private String[] latestConf;
    private String sponsers;
    private String sponsersinfo;
    private String headercontent;
    private String topics;
    private String journals;
    private String adeadline;
    private String pdeadline;
    /**
     * @return the conf_id
     */
    public String getConf_id() {
        return conf_id;
    }
    /*getter*/
    public model.Conference getConferenceInfo(int conf_id){
        String query="select * from conference.conference where conf_id="+conf_id;
        Conference conference=new Conference();
        try{
            java.sql.Connection conn=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=conn.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            
            while(rs.next()){
                conference.setAbstractDeadline(rs.getString("adeadline"));
                conference.setPaperDeadLine(rs.getString("pdeadline"));
                conference.setCompany(rs.getString("company"));
                conference.setConf_id(rs.getString("conf_id"));
                conference.setConf_name(rs.getString("conf_name"));
                conference.setDepartment(rs.getString("department"));
                conference.setDesc(rs.getString("description"));
                conference.setStart_date(rs.getString("start_date"));
                conference.setEnd_date(rs.getString("end_date"));
                conference.setHeaderContent(rs.getString("headercontent"));
                conference.setJournals(rs.getString("journals"));
                conference.setSponsers(rs.getString("sponsers"));
                conference.setSponsersInfo(rs.getString("sponsersinfo"));
                conference.setTopics(rs.getString("topics"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return conference;
    }
    
    public String getSponsers(){
        return this.sponsers;
    }
    
    public String getSponsersInfo(){
        return this.sponsersinfo;
    }
    
    public String getHeaderContent(){
        return this.headercontent;
    }
    
    public String getTopics(){
        return this.topics;
    }
    public String getJournals(){
        return this.journals;
    }
    public String getAbstractDeadline(){
        return this.adeadline;
    }
    public String getPaperDeadLine(){
        return this.pdeadline;
    }
    /*setter*/
    public void setSponsers(String sponsers){
        this.sponsers=sponsers;
    }
    
    public void setSponsersInfo(String sponsersinfo){
        this.sponsersinfo=sponsersinfo;
    }
    
    public void setHeaderContent(String content){
        this.headercontent=content;
    }
    
    public void setTopics(String content){
        this.topics=content;
    }
    public void setJournals(String content){
        this.journals=content;
    }
    public void setAbstractDeadline(String content){
        this.adeadline=content;
    }
    public void setPaperDeadLine(String content){
        this.pdeadline=content;
    }
    
    public String getAllPaidConferences(int userid){
        String query="select distinct conf_id from conference.payment where user_id="+userid;
        String allPaidConfIds="";
        try{
            java.sql.Connection conn=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=conn.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                if(allPaidConfIds.equals("")){
                    allPaidConfIds=rs.getString("conf_id");
                }else{
                    allPaidConfIds+=","+rs.getString("conf_id");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return allPaidConfIds;
    }
    public java.util.ArrayList<String[]> getAllConferenceNames(){
        java.util.ArrayList<String[]> list=new java.util.ArrayList<String[]>();
        String query="select conf_id,conf_name from conference.conference order by conf_id";
        try{
            java.sql.Connection conn=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=conn.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                list.add(new String[]{
                    rs.getString("conf_id"),
                    rs.getString("conf_name")
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return list;
    } 
    
    /**
     * @param conf_id the conf_id to set
     */
    public void setConf_id(String conf_id) {
        this.conf_id = conf_id;
    }

    /**
     * @return the conf_name
     */
    public String getConf_name() {
        return conf_name;
    }

    /**
     * @param conf_name the conf_name to set
     */
    public void setConf_name(String conf_name) {
        this.conf_name = conf_name;
    }

    /**
     * @return the start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }
    
    public int createConference(String qry) throws SQLException, ClassNotFoundException{
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(qry);
        int result;
        if(!ps.execute())
            result=1;
        else
            result=0;
        
        return result;
        
    }
   
    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public ArrayList<String[]> getallconfdetails(String query) throws SQLException, ClassNotFoundException{
        ArrayList<String[]> conflist=new ArrayList<String[]>(); 
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            conflist.add(new String[]{
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7)
                                
            });
            System.out.println(rs.getString(1)+" "+rs.getString(3)+" "+rs.getString(5));
        }
        return conflist;
     }
    
    public String[] getconfdetail(String query) throws SQLException, ClassNotFoundException{
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
         return (
                        new String[]{
                            rs.getString(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(7)
                          }
                        );
        
        }
        return null;
        
       
        
    }
    @Override
    public String[]  getLatestNews(String qry){
        
        try{
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(qry);
        ResultSet rs=ps.executeQuery();
        latestConf = new String[3];
        while(rs.next()){
         
         latestConf[0]=rs.getString("conf_name");
                latestConf[1]=rs.getString("start_date");
                latestConf[2]=rs.getString("description");
         }
        }
        catch(SQLException se){
            se.printStackTrace();
        }
           return latestConf;
        }
    
    
    public ArrayList<String[]> getTopConf(String qry) throws SQLException{
        
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(qry);
        ResultSet rs=ps.executeQuery();
        ArrayList<String[]> topList = new ArrayList<String[]>();
        while(rs.next()){
            topList.add(new String[]{
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                                   
            });
        }
        return topList;
    }
    
    public ArrayList<String> getConfidofuser(int uid){
       ArrayList<String> confids=new ArrayList<String>();
       String q="select distinct(paper.conf_id),conf_name from paper,conference where paper.conf_id=conference.conf_id and user_id="+uid;
        try{
            java.sql.Connection con= SingleDBObject.getConnectionObject();
               java.sql.PreparedStatement ps = con.prepareStatement(q);
                ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    confids.add(rs.getString(1)+"_"+rs.getString(2));
                }
        
        
        }
        catch(Exception e){
            e.printStackTrace();;
        }
        return confids;
        
    }
            
    public boolean insertpayment(String confid,String uid){
        boolean result=false;
        java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
java.util.Date date = new java.util.Date();
String d=dateFormat.format(date).toString();
        
        
        String q="insert into payment values(0,"+uid+","+confid+",500,'"+d+"')";
        try{
             java.sql.Connection con= SingleDBObject.getConnectionObject();
                java.sql.PreparedStatement ps = con.prepareStatement(q);
               if(!ps.execute())
                   result=true;
               else
                   result=false;
                     
              
        }
        catch(Exception e){
            e.printStackTrace();
        }
    return result;
    }
    public ArrayList<String> checkpayment(ArrayList<String> a){
       
        ArrayList<String> aFinal=new ArrayList<String>();
        try{
        java.sql.Connection con= SingleDBObject.getConnectionObject();
        for(int j=0;j<a.size();j++){
           String[] temp=a.get(j).split("_");
           String q="select user_id from payment where conf_id="+temp[0];
               java.sql.PreparedStatement ps = con.prepareStatement(q);
               ResultSet rs=ps.executeQuery();
               if(rs.last())
                   aFinal.add("true");
               else
                   aFinal.add("false");
           
       }
        
       }
       catch(Exception e){
           e.printStackTrace();
       }
        return aFinal;
        
    }
}



/*Below is the before code*/
/*

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Conference {
    private String conf_id;
    private String conf_name;
    private String start_date;
    private String end_date;
    private String department;
    private String company;
    private String desc;

    /**
     * @return the conf_id
     
    public String getConf_id() {
        return conf_id;
    }

    /**
     * @param conf_id the conf_id to set
     
    public void setConf_id(String conf_id) {
        this.conf_id = conf_id;
    }

    /**
     * @return the conf_name
     
    public String getConf_name() {
        return conf_name;
    }

    /**
     * @param conf_name the conf_name to set
     
    public void setConf_name(String conf_name) {
        this.conf_name = conf_name;
    }

    /**
     * @return the start_date
     
    public String getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    /**
     * @return the department
     
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the company
     
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     
    public void setCompany(String company) {
        this.company = company;
    }
    
    public int createConference(String qry) throws SQLException, ClassNotFoundException{
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(qry);
        int result;
        if(!ps.execute())
            result=1;
        else
            result=0;
        
        return result;
        
    }
   
    /**
     * @return the desc
     
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public ArrayList<String[]> getallconfdetails(String query) throws SQLException, ClassNotFoundException{
        ArrayList<String[]> conflist=new ArrayList<String[]>(); 
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            conflist.add(new String[]{
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7)
                                
            });
            System.out.println(rs.getString(1)+" "+rs.getString(3)+" "+rs.getString(5));
        }
        return conflist;
     }
    
    public String[] getconfdetail(String query) throws SQLException, ClassNotFoundException{
        Connection con= SingleDBObject.getConnectionObject();
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
         return (
                        new String[]{
                            rs.getString(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(7)
                          }
                        );
        
        }
        return null;
        
       
        
    }
}
*/