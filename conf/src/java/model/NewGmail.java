package model;

import static java.lang.ProcessBuilder.Redirect.to;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class NewGmail implements Email {
    
    private String from;
    private String to;
    private String subject;
    private String body;
    InternetAddress[] toAddr;

    public InternetAddress[] setToAddr(String[] toEmailAddr) throws AddressException {
        toAddr = new InternetAddress[toEmailAddr.length];
        for (int ndx = 0; ndx < toEmailAddr.length; ++ndx) {
            toAddr[ndx] = new InternetAddress(toEmailAddr[ndx]);

        }

        return toAddr;
    }

    
    @Override
    public void sendEmail() {

        int port = 465;
        String host = "smtp.gmail.com";
        boolean auth = true;
        final String username = "rambop91@gmail.com";
        final String password = "manbhigi";
        Prots protocol = Prots.SMTPS;
        boolean debug = true;

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        switch (protocol) {
            case SMTPS:
                props.put("mail.smtp.ssl.enable", true);
                break;
            case TLS:
                props.put("mail.smtp.starttls.enable", true);
                break;
        }
        Authenticator authenticator = null;
        if (auth) {
            props.put("mail.smtp.auth", true);
            authenticator = new Authenticator() {
                private PasswordAuthentication pa = new PasswordAuthentication(username, password);

                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return pa;
                }
            };
        }
        Session session = Session.getInstance(props, authenticator);
        session.setDebug(debug);

        MimeMessage message;
        message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(getFrom()));
            String[] totem = new String[2];
            totem[0] = "patelatit2012@gmail.com";
            totem[1] = "h2013194@pilani.bits-pilani.ac.in";

            String[] torecipient = getTo().split(",");
            for (String a : torecipient) {
                System.out.println(a);
            }

            InternetAddress[] tAddr = setToAddr(torecipient);
            for (int i = 0; i < tAddr.length; i++) {
                message.addRecipient(Message.RecipientType.TO, tAddr[i]);
            }

            BodyPart messageBodyPart1 = new MimeBodyPart();

            //5) create Multipart object and add MimeBodyPart objects to this object      
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);

    //6) set the multiplart object to the message object  
            message.setContent(multipart);
            System.out.println("Body "+getBody());
            messageBodyPart1.setText(getBody());
            message.setSubject(getSubject());
            message.setSentDate(new Date());
            
            Transport.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        NewGmail nw = new NewGmail();
        nw.setBody("heye");
        nw.setSubject("Game te");
        nw.setTo("rambop91@gmail.com");
        nw.setFrom("rambop91@gmail.com");
        nw.sendEmail();
        //nw.sendEmail("rambop91@gmail.com", "patelatit2012@gmail.com,rambop91@gmail.com", "Subject", "Heyy");
    }

    @Override
    public void setFilename(String filename) {
        filename=null;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    @Override
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    @Override
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    @Override
    public void setBody(String body) {
        this.body = body;
    }

    /**
     *
     * @param args
     */
    
    
}
