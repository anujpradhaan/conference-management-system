package model;

import java.sql.SQLException;

public class ProxyConference implements Conf{

    private Conference proxyConf;
    private String[] latestConf;

    @Override
    public String[] getLatestNews(String qry) {
        if (proxyConf == null)
        {
            proxyConf =  new Conference();
        }
        latestConf =new String[3];
        latestConf = proxyConf.getLatestNews(qry);
        return latestConf;
    }
}
