/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Dell
 */
public abstract class SubmitPaper extends Paper{
    
    public void setTitle(String title){
        this.title=title;
    }
    
    public void setStatus(int status){
        this.status=status;
    }
    
    public void setDirectoryStructure(String directory){
        this.directoryStructure=directory;
    }
    
    public void setDateOfSubmission(String date){
        this.dateOfSubmission=(new java.util.Date().toString());
    }
    
    public void setUserId(int userid){
        this.userid=userid;
    }
    
    public void setConferenceId(int id){
        this.conferenceId=id;
    }
    
    public void setPaperType(String type){
        this.type=type;
    }
    
    public abstract void saveFileDetailsToDB(); 
    
}
