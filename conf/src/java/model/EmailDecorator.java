

package model;


public class EmailDecorator implements Email{

    public Email decoratedEmail;
    private String from;
    private String to;
    private String subject;
    private String body;
    String filename;
    
    public EmailDecorator(Email e){
        this.decoratedEmail=e;
    }
    
    @Override
    public void sendEmail() {
        decoratedEmail.sendEmail();
    }

    
    
    @Override
    public void setFilename(String filename) {
        this.filename=filename;
    }

    @Override
    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public void setFrom(String from) {
        this.from=from;
    }

    @Override
    public void setSubject(String sub) {
        this.subject=sub;
    }

    @Override
    public void setBody(String body) {
        this.body=body;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }
    
}
