package model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dell
 */
public class UserModel {
    java.sql.Connection connection;
    public UserModel(){
        if(SingleDBObject.SINGLE_CONNECTION_OBJECT!=null)
            connection=SingleDBObject.SINGLE_CONNECTION_OBJECT;
        else{
            SingleDBObject.getSingleConnectionObject();
            connection=SingleDBObject.SINGLE_CONNECTION_OBJECT;
        }
    }
    
    public void signUpUser(String query){
        try{
        java.sql.Statement stmt=connection.createStatement();
        stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public String[] authenticateUser(String query){
        try{
            java.sql.Statement stmt=connection.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                return (
                        new String[]{
                            rs.getString("id"),
                            rs.getString("firstname"),
                            rs.getString("lastname"),
                            rs.getString("email"),
                            rs.getString("dob"),
                            rs.getString("interestarea"),
                            rs.getString("company"),
                            rs.getString("type_of_user"),
                          }
                        );
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public String[] getUserDetails(int userid){
        String userinfo[]=null;
        java.sql.Connection connection=SingleDBObject.getConnectionObject();
        try{
        java.sql.Statement stmt=connection.createStatement();
        String query="select * from conference.user where user_id="+userid;
        System.out.println(query);
        java.sql.ResultSet rs=stmt.executeQuery(query);
        while(rs.next()){
            return new String[]{
                rs.getString("user_id"),
                rs.getString("fname"),
                rs.getString("lname"),
                rs.getString("emailid"),
                rs.getString("dob"),
                rs.getString("areasofinterest"),
                rs.getString("company"),
                rs.getString("user_type"),
            };
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return userinfo;
    }
    public void updateUserProfile(int userid,String[] userinfo){
       String query="";
       query="set fname='"+userinfo[0]+"'";
       query+=",lname='"+userinfo[1]+"'";
       query+=",emailid='"+userinfo[2]+"'";
       query+=",dob='"+userinfo[3]+"'";
       query+=",areasofinterest='"+userinfo[4]+"'";
       query+=",company='"+userinfo[5]+"'";
       query="update conference.user "+query+" where user_id="+userid;
       try{
        java.sql.Statement stmt=connection.createStatement();
        stmt.execute(query);
       }catch(Exception e){
           e.printStackTrace();
       }
    }
   
}
