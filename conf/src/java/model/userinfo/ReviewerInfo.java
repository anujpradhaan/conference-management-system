/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.userinfo;

import model.SingleDBObject;

/**
 *
 * @author Dell
 */
public class ReviewerInfo extends UserInfo{
    
    public ReviewerInfo(){
    
    }
    
    public ReviewerInfo(int id){
        getUserInfo(id);
    }
    
    @Override
    public void getUserInfo(int id) {
        String query="select * from conference.user where user_id="+id;
        try{
            java.sql.Connection connection=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                this.setEmailId((rs.getString("emailid")));
                this.setFname(rs.getString("fName"));
                this.setLname(rs.getString("lName"));
                this.setUserCompany(rs.getString("company"));
                this.setUserId(Integer.parseInt(rs.getString("user_id")));
                this.setUserType(Integer.parseInt(rs.getString("user_type")));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
