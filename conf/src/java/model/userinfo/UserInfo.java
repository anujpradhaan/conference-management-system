/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.userinfo;

/**
 *
 * @author Dell
 */
public abstract class UserInfo {
    public int usertype;
    public String fName;
    public String lName;
    public String emailid;
    public int userid;
    public String userCompany;
    
    public abstract void getUserInfo(int userid);
    
    public void setUserType(int type){
        this.usertype=type;
    }
    
    public void setFname(String fname){
        this.fName=fname;
    }
    
    public void setLname(String lname){
        this.lName=lname;
    }
    
    public void setEmailId(String emailid){
        this.emailid=emailid;
    }
    
    public void setUserId(int userid){
        this.userid=userid;
    }
    
    public void setUserCompany(String company){
        this.userCompany=company;
    }
    
    /*Getter Method below*/
    public int getUserType(){
        return this.usertype;
    }
    
    public String getFname(){
        return this.fName;
    }
    
    public String getLname(){
        return this.lName;
    }
    
    public String getEmailId(){
        return this.emailid;
    }
    
    public int getUserId(){
        return this.userid;
    }
    
    public String getUserCompany(){
        return this.userCompany;
    }
}
