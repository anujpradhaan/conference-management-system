package model;

import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class AttachementsEmailDecorator extends EmailDecorator {

    InternetAddress[] toAddr;
    private String filename;
    private String from;
    private String to;
    private String subject;
    private String body;

    public AttachementsEmailDecorator(Email e) {
        super(e);
    }

    public void sendEmail() {
        
        sendAttachments();
    }

    public InternetAddress[] setToAddr(String[] toEmailAddr) throws AddressException {
        toAddr = new InternetAddress[toEmailAddr.length];
        for (int ndx = 0; ndx < toEmailAddr.length; ++ndx) {
            toAddr[ndx] = new InternetAddress(toEmailAddr[ndx]);

        }

        return toAddr;
    }

    public void sendAttachments() {
        System.out.println("Come here");
        int port = 465;
        String host = "smtp.gmail.com";
        boolean auth = true;
        final String username = "rambop91@gmail.com";
        final String password = "manbhigi";
        Prots protocol = Prots.SMTPS;
        boolean debug = true;

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        switch (protocol) {
            case SMTPS:
                props.put("mail.smtp.ssl.enable", true);
                break;
            case TLS:
                props.put("mail.smtp.starttls.enable", true);
                break;
        }
        
        System.out.println("Till here");
        Authenticator authenticator = null;
        if (auth) {
            props.put("mail.smtp.auth", true);
            authenticator = new Authenticator() {
                private PasswordAuthentication pa = new PasswordAuthentication(username, password);

                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return pa;
                }
            };
        }
        Session session = Session.getInstance(props, authenticator);
        session.setDebug(debug);
        System.out.println("Till there");
        MimeMessage message;
        message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(getFrom()));
            //InternetAddress[] address = {new InternetAddress("patelatit2012@gmail.com,h2013194@pilani.bits-pilani.ac.in")};
            String[] totem = new String[2];
            totem[0] = "patelatit2012@gmail.com";
            totem[1] = "h2013194@pilani.bits-pilani.ac.in";

            System.out.println("To "+getTo());
            String[] torecipient = getTo().split(",");
            for (String a : torecipient) {
                System.out.println(a);
            }

            InternetAddress[] tAddr = setToAddr(torecipient);
            for (int i = 0; i < tAddr.length; i++) {
                message.addRecipient(Message.RecipientType.TO, tAddr[i]);
            }
            //message.addRecipients(Message.RecipientType.TO, address);
            //message.setRecipients(Message.RecipientType.TO, address);
            BodyPart messageBodyPart1 = new MimeBodyPart();
            //messageBodyPart1.setText(body);
            /// Ahi thi 2nd
            //4) create new MimeBodyPart object and set DataHandler object to this object      
    /* ahi thi lai lidhu */

            MimeBodyPart messageBodyPart2 = new MimeBodyPart();

            //String filename = "I:\\Readme.txt";//change accordingly  
            System.out.println("File name "+getFilename());
            DataSource source = new FileDataSource(getFilename());
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(getFilename());

            //5) create Multipart object and add MimeBodyPart objects to this object      
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);
    // aa biju
            multipart.addBodyPart(messageBodyPart2);  

    //6) set the multiplart object to the message object  
            /// Aiha thi 3rd
            message.setContent(multipart);
            System.out.println("Body "+getBody());
            messageBodyPart1.setText(getBody());
            message.setSubject(getSubject());
            message.setSentDate(new Date());
            // message.setText(body);
            System.out.println("Bdhu thai gyu.");
            Transport.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        if(filename.equals("")||filename==null)
            filename=" ";
        this.filename = filename;
        
    }

}
