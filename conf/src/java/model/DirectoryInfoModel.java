/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class DirectoryInfoModel {
    
    public static String getUploadDirectory(){
        return "";
    }
    
    public static String getNewReviewerAccountMessage(){
        return "A Reviewer account at BITS COnference Management System is created for you. Kindly update your profile"
                + ". And in case of any difficulties contact us at rambop91@gmail.com"
                + " You can Login At http://localhost:9090/conf/login.jsp";
    }
    public static String getpapersubmissiondescription(){
        return " We have received your paper. We will review your paper as soon as possible.We will revert back as soon as paper is reviewed."+
                " You can also have a conversation with the admin for the respective paper";
    }
    
    public static String getabstractsubmissiondescription(){
        return " We have received your abstract. We will review your abstract Ans will revert back as soon as paper is reviewed."+
                " You can also have a conversation with the admin for the respective paper";
    }
    
    public static String chairEmailId(){
        return "rambop91@gmail.com";
    }
    public static String ReviewCommentAddedContent(){
        return "Reviewer has commented on your paper. Kindly check it and do reply according to it asap ";
    }
    public static String reviewerAssignedMsg(){
        return "A Paper has been assigned to you in the BITS COnference management system.Please review it and evaluate it accordingly";
    }
    
    public static String getPaperRejectionMsg(){
        return "Your Paper/Abstract has Been rejected By our Organizers.";
    }
    
    public static String getRequestToreassignBody(String title){
        return "The Reviewer asked for reassignment of paper Titled::"+title+". Reason:::>>";
    }
}
