/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class Reviewer extends User{
    java.sql.Connection connection;
    java.sql.Statement stmt;
    EmailAdapter emailAdapter;
    public Reviewer(){
        this.connection=SingleDBObject.getConnectionObject();
        createStatement();
    }
    
    private void createStatement(){
        try{
        stmt=connection.createStatement();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void updateProfile(){
        
    }
    
    public void viewPaper(int paperId){
        
    }
    
    public void sendMail(int userid,String emailType){
        //emailAdapter = new EmailAdapter(emailType);
    }
    
    public void sendMail(int []userids,String emailType){
        //emailAdapter = new EmailAdapter(emailType);
    }
    
    public void postComment(Paper paper,String comment){
        
    }
    
    public void requestToReassign(Paper paper){
    
    }
    
    public void writeReview(Paper paper){
        
    }
    
    public void evaluatePaper(Paper paper,int status,int points){
    
    }
    
    public java.util.List<PaperInfo> getAllNewPapersForReviewer(int userid){//papers which are not even assigned to anyone
        String query;
        query="select * from conference.paper where active=1 and reviewed_by="+userid;
        System.out.println(query);
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           
           return getPaperObject(rs);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    private java.util.List<PaperInfo> getPaperObject(java.sql.ResultSet rs){//this function will create a arraylist of paper type objects 
        java.util.ArrayList<PaperInfo> pInfo=new java.util.ArrayList<PaperInfo>();
        try{
        while(rs.next()){
            PaperInfo paperinfo=null;
            if(rs.getString("typeofpaper").equals("abstract"))
                paperinfo=new AbstractInfo(Integer.parseInt(rs.getString("paper_id")));
            else
                paperinfo=new FinalPaperInfo(Integer.parseInt(rs.getString("paper_id")));
            
            pInfo.add(paperinfo);
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return pInfo;
    }
    
    public java.util.ArrayList<String[]> getAllReviewers(){
        String query="select concat(fname,' ',lname) as name,user_id from conference.user where user_type=2";
        java.util.ArrayList<String[]> list=new java.util.ArrayList<String[]>();
        try{
            java.sql.Connection connection = model.SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                list.add(new String[]{
                    rs.getString("name"),rs.getString("user_id")
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return list;
    }
    
    public void deleteReviewer(int reviewerId){
        String query="delete from conference.user where user_id="+reviewerId;
        try{
            java.sql.Connection connection = model.SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}