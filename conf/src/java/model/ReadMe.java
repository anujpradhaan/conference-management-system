/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class ReadMe {
    /*
     This class provides information about all the models present in this folder.
     * 
     * Whenever you want to fetch a paper you need to create a correcponding object.
     * The reason for this will be MVC design. Since we want to show the model state to view.
     * We will Maintain the paper in form of an object and it will contain all the information related to itself.
     * This we we just need to pass an objec to the view and view will fetch all the information accordingly.
     * class file for this is Named PaperInfo. This will be a interface and it will be extended by Two classes.
     * 1)AbstractInfo
     * 2)FinalPaperInfo
     */
    
    /*
     Classes like Chair,Reviewer,User are build to perform user operations.
     * To get particular Chair,Reviewer,User Type object three separate classes are designed.
     * 1)UserInfo => Interface
     * 2)AuthorInfo =>author class
     * 3)ChairInfo =>chair class
     * 4)ReviewerInfo =>Reviewer Class
     */
}
