package model;

public class EmailAdapter {

    Email email;

    public EmailAdapter(String emailType,String to,String from,String subject,String body) {
        //If you do not want to attach anything set filename = null.
        if (emailType.equalsIgnoreCase("Gmail")) {
            Email email = new AttachementsEmailDecorator(new NewGmail());
            email.setTo(to);
            email.setFrom(from);
            email.setBody(body);
            email.setSubject(subject);
            email.setFilename("C:\\Users\\Public\\Pictures\\Sample Pictures\\bits_logo.jpg");
            email.sendEmail();
            
        }
        else{
            Email email = new AttachementsEmailDecorator(new NewYmail());
            email.setTo(to);
            email.setFrom(from);
            email.setBody(body);
            email.setSubject(subject);
            email.setFilename("");
            email.sendEmail();
        }
            
    }

}
