/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class FetchPaper {
 
    java.sql.Connection connection;
    public FetchPaper(){
        this.connection=SingleDBObject.getConnectionObject();
    }
    
    public String getDirectoryStructure(){
        return null;
    }
    
    public String getTitle(){
        return null;
    }
    
    public int getStatus(){
        return 0;
    }
    
    public java.util.List getPaperInfo(int userid){
        String paperInfo[]=null;
        java.util.ArrayList<String[]> list=new java.util.ArrayList<String[]>();
        String query="select * from paper where User_id="+userid+" and active != 0";
        try{
            java.sql.Statement stmt=connection.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            java.sql.ResultSetMetaData rsmd=rs.getMetaData();
            
            while(rs.next()){
                paperInfo=new String[rsmd.getColumnCount()];
                for(int i=0;i<paperInfo.length;i++){
                    if(i==7)//means file path column from the database
                        paperInfo[i]=DirectoryInfoModel.getUploadDirectory()+rs.getString(i+1);
                    else    
                        paperInfo[i]=rs.getString(i+1);
                }
                list.add(paperInfo);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return list;
    }
}
