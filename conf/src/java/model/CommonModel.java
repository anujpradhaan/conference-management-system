/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Dell
 */
public class CommonModel {
    java.sql.Connection connection;
    java.sql.Statement stmt;
    public CommonModel(){
        connection=SingleDBObject.getConnectionObject();
        createStatement();
    }
    
    private void createStatement(){
        try{
        stmt=connection.createStatement();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public String getPaperTitle(int paperId){
        String query="select title from conference.paper where paper_id="+paperId;
        try{
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                return rs.getString("title");
            }
        }catch(Exception e){
          e.printStackTrace();      
        }
        return "No Title Available";
    }
    public String getAnyUserEmailId(int userid){
        String query="select emailid from conference.user u where u.user_id="+userid;
        String emailid="rambop91@gmail.com";
        try{
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next())
                return rs.getString("emailid");
        }catch(Exception e){
            e.printStackTrace();
        }
        return emailid;
    }
    public String getAuthorEmailId(int paperId){
        String query="SELECT emailid FROM conference.paper p,conference.user u where u.user_id=p.User_id and p.paper_id="+paperId;
        String emailid="rambop91@gmail.com";
        try{
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                return rs.getString("emailid");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return emailid;
    }
    
    public void makeComment(int paperId,int userId,String comment){
        String query="insert into conference.comment(user_id,user_id_r,comment_desc2,paper_id) values("+userId+","+userId+",'"+comment+"',"+paperId+")";
        try{
            this.stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //public java.util.ArrayList<>
    public java.util.ArrayList<model.userinfo.UserInfo> getAllReviewerInfo(){
        String query="select user_id from conference.user where user_type=2";
        java.util.ArrayList<model.userinfo.UserInfo>  userList=new java.util.ArrayList<model.userinfo.UserInfo>();
        try{
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                userList.add(new model.userinfo.ReviewerInfo(Integer.parseInt(rs.getString("user_id"))));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return userList;
    }
    
    public void assignReviewerToPaper(int paperid,int reviewerid){
        String query="update conference.paper set reviewed_by="+reviewerid+",paper_status=1 where paper_id="+paperid;
        try{
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public boolean doevalution (int marks,int paperid){
        String query="update conference.paper set points="+marks+", paper_status=2 where paper_id="+paperid;
        boolean result=false;
        try{
            if(!stmt.execute(query))
                result=true;
            else
                result=false;
        }
        catch(Exception e){
            e.printStackTrace();;
        }
        return result;
    }
    
    public void acceptPaper(int paperid){
        String query="update conference.paper set paper_status=3 where paper_id="+paperid;
        try{
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void rejectPaper(int paperid){
        String query="update conference.paper set paper_status=4,active=0 where paper_id="+paperid;
        try{
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void reassignPaper(int userid,int paperid){
        String query="update conference.paper set paper_status=0,reviewed_by=0 where paper_id="+paperid;
        try{
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<String[]> getAllPaperComment(int paperId)  {
      
        ArrayList<String[]> cmntlist=new ArrayList<String[]>();
        try{
        ResultSet resultcmnt= stmt.executeQuery("SELECT user_type,concat(fname,' ',lname) as name,c.Comment_desc2 as comment FROM comment c,user u where c.user_id=u.user_id and c.paper_id="+paperId);
        while(resultcmnt.next()){
            cmntlist.add( new String[]{
                resultcmnt.getString(1),
                resultcmnt.getString(2),
                resultcmnt.getString(3),
            });
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return cmntlist;
        
    }

    public ArrayList<String> fetchreviewrid(){
        String query="select user_id,fname from user where user_type=2";
        ArrayList<String> uids=new ArrayList<String>();
        
        try{
        ResultSet userids=stmt.executeQuery(query);
        while(userids.next()){
            uids.add(userids.getString(1)+"_"+userids.getString(2));
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return uids;
    }
    public ArrayList<String[]> fetchreviewersinfo() {
        ArrayList<String[]> allinfo=new ArrayList<String[]>();
        try{
       String query="SELECT title,submission_date,paper_status_content,fname, (select conf_name from conference.conference c where c.conf_id=p.conf_id) as conf_id,filepath,points,typeofpaper,reviewed_By,p.user_id,paper_id FROM conference.paper p,conference.user u,conference.paper_status_name where p.paper_status=paper_status_name.paper_status and p.User_id=u.User_id";    
        ResultSet userinfos=stmt.executeQuery(query);
        while(userinfos.next()){
            allinfo.add(new String[]{ 
                userinfos.getString(1),
                userinfos.getString(2),
                userinfos.getString(3),
                userinfos.getString(4),
                userinfos.getString(5),
                userinfos.getString(6),
                userinfos.getString(7),
                userinfos.getString(8),
                userinfos.getString(9),
                userinfos.getString(10),
                userinfos.getString(11)
           
            });
        System.out.println("uinfo="+userinfos.getString(1));
        
        }
        
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return allinfo;
    }
    
    
    
}
