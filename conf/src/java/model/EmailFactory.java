package model;


public class EmailFactory {

    private String EmailType;

    //use getShape method to get object of type shape 
    public Email getEmail(String EmailType) {
        if (EmailType == null) {
            return null;
        }
        if (EmailType.equalsIgnoreCase("GMAIL")) {
            return new NewGmail();
        } else if (EmailType.equalsIgnoreCase("YAHOO")) {
            return new NewYmail();
        }
        return null;
    }
}
