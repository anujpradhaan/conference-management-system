package model;


public interface Email {
    
public void sendEmail(); 
public void setFilename(String filename);
public void setTo(String to);
public void setFrom(String from);
public void setSubject(String sub);
public void setBody(String body);
//public void setElements();
}
