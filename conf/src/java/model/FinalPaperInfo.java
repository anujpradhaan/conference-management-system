/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class FinalPaperInfo implements PaperInfo{
    
    public FinalPaperInfo(){
    
    }
    
    public FinalPaperInfo(int paperid){
        setPaperDetails(paperid);
    }
    
    private int confId;
    private int paperId;
    private int paperStatus;
    private String paperTitle;
    private int userId;
    private String paperType;
    private String fileName;
    public int marks;
    private String conferenceName;
    private String authorName;
    private String reviewerName;
    
    private void setPaperDetails(int id){
        String query="select * from conference.paper where paper_id="+id+" and typeofpaper='finalpaper'";
        try{
            java.sql.Connection connection=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                this.setPaperTitle(rs.getString("title"));
                this.setConferenceId(Integer.parseInt(rs.getString("conf_id")));
                this.setPaperId(Integer.parseInt(rs.getString("paper_id")));
                this.setPaperStatus(Integer.parseInt(rs.getString("paper_status")));
                this.setUserId(Integer.parseInt(rs.getString("user_id")));
                this.setPaperType(rs.getString("typeofpaper"));
                this.setMarks(Integer.parseInt(rs.getString("points")));
                this.setConferenceName(this.getConferenceName(Integer.parseInt(rs.getString("conf_id"))));
                this.setAuthorName(this.getAuthorName(Integer.parseInt(rs.getString("user_id"))));
                this.setReviewerName(this.getReviewerName(Integer.parseInt(rs.getString("paper_id"))));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void setConferenceName(String name){
        this.conferenceName=name;
    }
    
    private void setAuthorName(String name){
        this.authorName=name;
    }
    private void setReviewerName(String name){
        this.reviewerName=name;
    }
    private String getConferenceName(int confid){
        String cName="";
        try{
            java.sql.Connection connection=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            String query="SELECT conf_name as cname FROM conference.conference where conf_id="+confid;
            //System.out.println("GetConference="+query);
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                cName=rs.getString("cname");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return cName;
    }
    
    private String getAuthorName(int userid){
        String name="";
        try{
            java.sql.Connection connection=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            String query="SELECT CONCAT(fname,\" \",lname) as name FROM USER where user_id="+userid;
            java.sql.ResultSet rs=stmt.executeQuery(query);
            
            while(rs.next()){
                name=rs.getString("name");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return name;
    }
    
    private String getReviewerName(int paperid){
        String rName="";
        try{
            java.sql.Connection connection=SingleDBObject.getConnectionObject();
            java.sql.Statement stmt=connection.createStatement();
            String query="SELECT CONCAT(fname,\" \",lname) as rname,paper_id FROM USER u,paper p WHERE u.user_id=p.reviewed_By and p.paper_id="+paperid;
            System.out.println(query);
            java.sql.ResultSet rs=stmt.executeQuery(query);
            
            while(rs.next()){
                rName=rs.getString("rname");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return rName;
    }
    
    private void setFileName(String name){
        this.fileName=name;
    }
    public String getFileName(){
        return this.fileName;
    }
    
    public void setConferenceId(int id){
        this.confId=id;
    }
    
    public void setPaperId(int id){
        this.paperId=id;
    }
    
    public void setPaperStatus(int status){
        this.paperStatus=status;
    }
    
    public void setPaperTitle(String title){
        this.paperTitle=title;
    }
    
    public void setUserId(int id){
        this.userId=id;
    }
    
    public void setPaperType(String type){
        this.paperType=type;
    }
    public void setMarks(int marks){
        this.marks=marks;
    }
    @Override
    public int getConferenceId() {
        return this.confId;
    }

    @Override
    public int getPaperId() {
        return this.paperId;
    }

    @Override
    public int getPaperStatus() {
        return this.paperStatus;
    }

    @Override
    public String getPaperTitle() {
        return this.paperTitle;
    }

    @Override
    public int getUserId() {
        return this.userId;
    }

    @Override
    public String getTypeOfPaper() {
        return this.paperType;
    }

    @Override
    public int getMarks() {
        return this.marks;
    }
    
    @Override
    public String getAuthorName() {
        return this.authorName;
    }

    @Override
    public String getConferenceName() {
        return this.conferenceName;
    }

    @Override
    public String getReviewerName() {
        return this.reviewerName;
    }
    
}
