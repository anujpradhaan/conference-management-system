/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import org.apache.commons.io.output.*;
/**
 *
 * @author Dell
 */
public class SubmitFinalPaper extends SubmitPaper{
    private String filename="";
    @Override
    public void saveFileDetailsToDB(){
      String query="insert into conference.paper(title,submission_date,paper_status,user_id,conf_id,filepath,typeofpaper,active) values('"+this.title+"',now(),"+this.status+","+this.userid+","+this.conferenceId+",'"+this.filename+"','"+this.type+"',1)";
      java.sql.Connection con=(SingleDBObject.getConnectionObject());
      try{
      java.sql.Statement stmt=con.createStatement();
      System.out.println(query);
      stmt.execute(query);
      }catch(Exception e){
          e.printStackTrace();
      }
    }
    
    public void updateFileDetailsToDB(int paperId){
      String query="update conference.paper set filepath='"+this.filename+"' where paper_id="+paperId;
      java.sql.Connection con=(SingleDBObject.getConnectionObject());
      try{
        java.sql.Statement stmt=con.createStatement();
        System.out.println(query);
        stmt.execute(query);
      }catch(Exception e){
          e.printStackTrace();
      }
    }
    
    public void setFileItems(java.util.List fileItems){
        java.io.File file ;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;
        java.util.Iterator i = fileItems.iterator();

         try {
         while ( i.hasNext () ) 
         {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () )	
            {
                // Get the uploaded file parameters
                String fieldName = fi.getFieldName();
                String fileName = fi.getName();
                this.filename=fileName;
                boolean isInMemory = fi.isInMemory();
                long sizeInBytes = fi.getSize();
                // Write the file
                if( fileName.lastIndexOf("\\") >= 0 ){
                file = new java.io.File( this.directoryStructure + 
                fileName.substring( fileName.lastIndexOf("\\"))) ;
                }else{
                file = new java.io.File( this.directoryStructure + 
                fileName.substring(fileName.lastIndexOf("\\")+1)) ;
                }
                fi.write( file );
            }
         }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
