/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public abstract class Paper {
    /*This class will be used only when a user is submitting his paper*/
    public String title;
    public int status;
    public String directoryStructure;
    public String dateOfSubmission;
    public int userid;
    public int conferenceId;
    public String type;
}
