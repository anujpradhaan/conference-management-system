/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public abstract class User {
    public String fname;
    public String lname;
    public String sex;
    public String dob;
    public String institute;
    public String company;
    public String[] areaOfInterest;
    public String email;
    public int type;
    
    public void setFName(String fname){
        this.fname=fname;
    }
    
    public void setLName(String lname){
        this.lname=lname;
    }
    
    public void setSex(String sex){
        this.sex=sex;
    }
    
    public void setDob(String dob){
        this.dob=dob;
    }
    
    public void setInstitute(String institute){
        this.institute=institute;
    }
    
    public void areaOfInterest(String[] areaofInterest){
        this.areaOfInterest=areaofInterest;
    }
    
    public void setEmailId(String email){
        this.email=email;
    }
    
    public void setUserType(int type){
        this.type=type;
    }
    
    public String getFName(){
        return this.fname;
    }
    
    public String getLName(){
        return this.lname;
    }
    
    public String getSex(){
        return this.sex;
    }
    
    public String getDob(){
        return this.dob;
    }
    
    public String getInstitute(){
         return this.institute;
    }
    
    public String[] getAreaOfInterest(){
        return this.areaOfInterest;
    }
    
    public String getEmailId(){
        return this.email;
    }
    
    public int getUserType(){
        return this.type;
    }
}
