/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class Chair extends User{
    java.sql.Connection connection;
    java.sql.Statement stmt;
    public Chair(){
        this.connection=SingleDBObject.getConnectionObject();
        createStatement();
    }
    
    private void createStatement(){
        try{
        stmt=connection.createStatement();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void sendMail(int userid){
        
    }
    
    public void sendMail(int[] userids){
        //To send group mails
    }
    
    public void assignPaper(Paper paper,Reviewer reviewer){
        
    }
    
    public void acceptPaper(Paper paper){
        //just nee dto change the status of the paper
    }
    
    public void rejectPaper(Paper paper){
        //just need to change the status of paper to rejected
    }
    
    public Paper[] viewSubmissions(Reviewer reviewer){
        return null;
    }
    
    public Paper[] viewSubmissions(Author author){
        return null;
    }
    
    public String getAllAuthorsEmailIds(){
        String query="select emailid from conference.user where user_type=0";
        String emails="";
        try{
        java.sql.ResultSet rs=stmt.executeQuery(query);
        while(rs.next()){
            if(emails.equals(""))
                emails=rs.getString("emailid");
            else
                emails+=","+rs.getString("emailid");
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return emails;
    }
    
    public String getAllReviewersEmailIds(){
        String query="select emailid from conference.user where user_type=2";
        String emails="";
        try{
        stmt.executeQuery(query);
        java.sql.ResultSet rs=stmt.executeQuery(query);
        while(rs.next()){
            if(emails.equals(""))
                emails=rs.getString("emailid");
            else
                emails+=","+rs.getString("emailid");
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return emails;
    }
    
    public boolean createReviewerAccount(String email,String password){
        String query="insert into conference.user (emailid,user_type,fname,lname,company,institute,areasofinterest) values ('"+email+"',2,'default','default','default','default','default')";
        System.out.println(query);
        boolean flag=false;
        try{
            stmt.execute(query);    
            query="insert into conference.login_details(username,password,user_id) select '"+email+"','"+password+"',user_id from user where emailid='"+email+"'";
//        /    System.out.println(query);
            flag=stmt.execute(query);    
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return flag;
    } 
    public void changePaperStatus(Paper paper,int status){
        //just change the status of this paper
    }
    
    public void createConference(){
        
    }
    
    public java.util.List<PaperInfo> getAllNewPapers(){//papers which are not even assigned to anyone
        String query;
        query="select * from conference.paper";
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           java.util.List<PaperInfo> temp=getPaperObject(rs);
          // System.out.println("sadasadsadadsad****"+temp);
           return temp;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    private java.util.List<PaperInfo> getPaperObject(java.sql.ResultSet rs){//this function will create a arraylist of paper type objects 
        java.util.ArrayList<PaperInfo> pInfo=new java.util.ArrayList<PaperInfo>();
        try{
        while(rs.next()){
            PaperInfo paperinfo=null;
            if(rs.getString("typeofpaper").equals("abstract")){
                paperinfo=new AbstractInfo(Integer.parseInt(rs.getString("paper_id").trim()));
                
            }else{
                //System.out.println("paperFor Final paper"+rs.getString("paper_id").trim());
                paperinfo=new FinalPaperInfo(Integer.parseInt(rs.getString("paper_id").trim()));
            }
            pInfo.add(paperinfo);
            
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return pInfo;
    }
    public java.util.List<PaperInfo> getAllPaperAccordingToTitle(String title){
        String query;
        query="SELECT * FROM conference.paper WHERE title LIKE '%"+title+"%' and active=1";
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           return getPaperObject(rs);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public java.util.List<PaperInfo> getAllPaperAccordingToAuthor(String title){
        String query;
        query="SELECT p.paper_id as paper_id,typeofpaper FROM conference.paper p,USER u WHERE p.User_id=u.User_id AND CONCAT(fname,\" \",lname) LIKE '%"+title+"%'";
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           return getPaperObject(rs);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public java.util.List<PaperInfo> getAllPaperAccordingToReviewer(String title){
        String query;
        query="SELECT p.paper_id as paper_id,typeofpaper FROM conference.paper p,USER u WHERE p.reviewed_by=u.User_id AND CONCAT(fname,\" \",lname) LIKE '%"+title+"%'";
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           return getPaperObject(rs);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    public java.util.List<PaperInfo> getAllPaperAccordingToConference(String title){
        String query;
        query="SELECT paper_id,typeofpaper FROM conference.paper p,conference.conference c where p.conf_id=c.conf_id and c.conf_name like '%"+title+"%'";
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           return getPaperObject(rs);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public java.util.List<PaperInfo> getAllPaperAccordingToPayment(String title){
        String query;
        query="SELECT * FROM conference.paper p,conference.payment pay where p.conf_id=pay.conf_id and typeofpaper='finalpaper' and p.title like '%"+title+"%'";
        System.out.println(query);
        try{
           java.sql.ResultSet rs=stmt.executeQuery(query);
           return getPaperObject(rs);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
}
