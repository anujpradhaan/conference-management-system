/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public interface PaperInfo {
    public String getPaperTitle();
    public int getPaperId();
    public int getPaperStatus();
    public int getUserId();
    public int getConferenceId();
    public String getTypeOfPaper();
    public String getFileName();
    public int getMarks();
    public String getAuthorName();
    public String getConferenceName();
    public String getReviewerName();
}
