/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dell
 */
public class SingleDBObject {
    public static java.sql.Connection SINGLE_CONNECTION_OBJECT=null;
    static{
        getSingleConnectionObject();
    }
    private SingleDBObject(){
        
    }
    
    public static void getSingleConnectionObject(){
        try{
            Class.forName("com.mysql.jdbc.Driver");  
             SINGLE_CONNECTION_OBJECT=(java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/conference", "root", ""));  
            }catch(java.sql.SQLException se){
                se.printStackTrace();
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public static java.sql.Connection getConnectionObject(){
        try{
            if(SINGLE_CONNECTION_OBJECT==null){
            Class.forName("com.mysql.jdbc.Driver");  
             SINGLE_CONNECTION_OBJECT=(java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/conference", "root", ""));  
            }
            }catch(java.sql.SQLException se){
                se.printStackTrace();
            }catch(Exception e){
                e.printStackTrace();
            }
        return SINGLE_CONNECTION_OBJECT;
    }
}
