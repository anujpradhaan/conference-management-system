/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dell
 */
public class UserModel {
    java.sql.Connection connection;
    public UserModel(){
        if(SingleDBObject.SINGLE_CONNECTION_OBJECT!=null)
            connection=SingleDBObject.SINGLE_CONNECTION_OBJECT;
        else{
            SingleDBObject.getSingleConnectionObject();
            connection=SingleDBObject.SINGLE_CONNECTION_OBJECT;
        }
    }
    public int signUpUser(String query){
        try{
        java.sql.Statement stmt=connection.createStatement();
        if(!stmt.execute(query))
        {
            System.out.println("Succesful");
            return 1;
        }
        else
        {
            System.out.println("Failed");
            return 0;
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return 0;
        
    }
    /*
    public void signUpUser(String query){
        try{
        java.sql.Statement stmt=connection.createStatement();
        stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }*/
    
    public String[] authenticateUser(String query){
        try{
            java.sql.Statement stmt=connection.createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                return (
                        new String[]{
                            rs.getString("user_id"),
                            rs.getString("fname"),
                            rs.getString("lname"),
                            rs.getString("emailid"),
                            rs.getString("dob"),
                            rs.getString("areasofinterest"),
                            rs.getString("company"),
                            rs.getString("user_type"),
                          }
                        );
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
