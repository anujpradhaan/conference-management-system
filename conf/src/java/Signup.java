/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Dell
 */
public class Signup extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
             
          //  out.println("before getting param");
           String fname=request.getParameter("fname");
           String lname=request.getParameter("lname");
           String sex=request.getParameter("s");
           String email=request.getParameter("email");
           String dob=request.getParameter("dob");
           String ic=request.getParameter("inst");
           String username=request.getParameter("username");
           String password=request.getParameter("password");
           String interest=request.getParameter("interest");
           
         System.out.println("fname="+fname);
          
           String query="insert into user values (0,0,'"+fname+"','"+lname+"','"+sex+"','"+dob+"','"+ic+"','"+ic+"','"+interest+"','"+email+"')";
         //  out.println("Query "+query);
          // query="insert into user(usertype,fname,lname,email,username,dob,interestarea,company) values ("+query+")";
           UserModel uModel=new UserModel();
           int flag= uModel.signUpUser(query);
           
           String login= "insert into login_details (Sr_no,Username,Password,User_id)  select 0,\""+email+"\",\""+password+"\",user_id from user where EMAILID ='"+email+"'";
                  
           int flag2=uModel.signUpUser(login);
           if(flag==1&&flag2==1){
               JSONObject obj=new JSONObject();
                obj.put("success","success");
                out.print(obj);
                out.flush();
           }
           else
           {
               JSONObject obj=new JSONObject();
                obj.put("success","failed");
                out.print(obj);
                out.flush();
           }
        } catch(Exception e){
            e.printStackTrace();
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
