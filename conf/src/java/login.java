/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
/**
 *
 * @author Dell
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           String []login=request.getParameterValues("login");
           String query="";
           query="select u.* from conference.login_details l,conference.user u where l.user_id=u.user_id and username='"+login[0]+"' and password='"+login[1]+"'";
           UserModel uModel=new UserModel();
           String []userinfo=uModel.authenticateUser(query);
           if(userinfo==null){
               //response.sendRedirect("index.jsp");
               HttpSession session=request.getSession();
               session.setAttribute("userid",null);
               JSONObject obj=new JSONObject();
                obj.put("success","failed");
                out.print(obj);
                out.flush();
           }else{
               HttpSession session=request.getSession();
               session.setAttribute("validuser",userinfo[1]+" "+userinfo[2]);
               session.setAttribute("userid",userinfo[0]);
               session.setAttribute("usertype",userinfo[7]);//0=>author 1=>chair 2=>reviewer
               session.setAttribute("userinfo",userinfo[0]+";"+userinfo[1]+";"+userinfo[2]+";"+userinfo[3]+";"+userinfo[4]+";"+userinfo[5]+";"+userinfo[6]);
               session.setAttribute("user_email",userinfo[3] );
               JSONObject obj=new JSONObject();
                obj.put("success","success");
                obj.put("firstname",userinfo[0]);
                obj.put("lastname",userinfo[1]);
                obj.put("email",userinfo[2]);
                obj.put("dob",userinfo[3]);
                obj.put("interestarea",userinfo[4]);
                obj.put("company",userinfo[5]);
                obj.put("typeofuser",userinfo[6]);
                out.print(obj);
                out.flush();
           }
        } catch(Exception e){
            e.printStackTrace();
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
