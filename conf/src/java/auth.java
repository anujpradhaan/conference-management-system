/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax

/**
 *
 * @author Dell
 */
@WebServlet(name = "auth", urlPatterns = {"/auth"})
public class auth extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet auth</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet auth at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
            /*
             On the basis of user type redirect to the particular home page.
             * For Author => index.jsp
             * for chair =>admindashboard.jsp
             * for reviewer => reviwerdash.jsp
             */
            if(request.getSession().getAttribute("validuser")==null){
                response.sendRedirect("login.jsp");
                return;
            }
            String type=(String)request.getSession().getAttribute("usertype");
            int usertype=Integer.parseInt(type);
            String id=(String)request.getSession().getAttribute("userid");
            int userId=Integer.parseInt(id);
            String viewpagename="index.jsp";
            switch(usertype){
                case 0://means author
                    viewpagename="index.jsp";
                    break;
                case 1:
                    viewpagename="chairdashboard.jsp";
                    model.Chair chair=new model.Chair();
                    //System.out.println(chair.getAllNewPapers());
                    request.setAttribute("paperinfo",chair.getAllNewPapers());
                    //response.setA
                  //  request.
                    break;
                case 2:
                    model.Reviewer reviewer=new model.Reviewer();
                    //String 
                    System.out.println(reviewer.getAllNewPapersForReviewer(userId));
                    request.setAttribute("paperinfo",reviewer.getAllNewPapersForReviewer(userId));
                    viewpagename="reviewerdashboard.jsp";
                    break;
            }
            javax.servlet.RequestDispatcher dispatcher = request.getRequestDispatcher(viewpagename);
            dispatcher.forward(request, response);
           // response.sendRedirect(viewpagename);
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
