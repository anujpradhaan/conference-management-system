/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author manish
 */
public class CreateConferece extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            model.Conference conf = new model.Conference();
            
            String topics=request.getParameter("topics").replaceAll("\n", " ");
            String sponsers=request.getParameter("sponsers").replaceAll("\n", " ");
            String sponsersinfo=request.getParameter("sponsersinfo").replaceAll("\n", " ");
            String journals=request.getParameter("journals").replaceAll("\n", " ");
            String headercontent=request.getParameter("headercontent").replaceAll("\n", " ");
            String adeadline=request.getParameter("adeadline");
            String pdeadline=request.getParameter("pdeadline");
            conf.setAbstractDeadline(adeadline);
            conf.setPaperDeadLine(pdeadline);
            conf.setHeaderContent(headercontent);
            conf.setJournals(journals);
            conf.setSponsers(sponsers);
            conf.setSponsersInfo(sponsersinfo);
            conf.setTopics(topics);
            conf.setConf_name(request.getParameter("cname"));
            conf.setCompany(request.getParameter("compname"));
            conf.setDepartment(request.getParameter("dname"));
            conf.setStart_date(request.getParameter("sdate"));
            conf.setEnd_date(request.getParameter("edate"));
            conf.setDesc(request.getParameter("desc"));
            String qry = "insert into conference values (0,'"+conf.getConf_name()+"','"+conf.getDepartment()+"','"+conf.getStart_date()+"','"+conf.getEnd_date()+"','"+conf.getCompany()+"','"+conf.getDesc()+
                   "','"+topics+"','"+journals+"','"+sponsers+"','"+sponsersinfo+"','"+headercontent+"','"+adeadline+"','"+pdeadline+"')";
           // out.println(qry);
            
           int flag= conf.createConference(qry);
           
          // out.println("flag="+flag);
           if(flag==1){
               JSONObject obj=new JSONObject();
                obj.put("success","success");
                out.print(obj);
                out.flush();
           }
           else{
               
               JSONObject obj=new JSONObject();
                obj.put("success","failure");
                out.print(obj);
                out.flush();
           
           }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(CreateConferece.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CreateConferece.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(CreateConferece.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CreateConferece.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
