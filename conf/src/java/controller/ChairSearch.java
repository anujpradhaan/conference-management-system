/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ATIT
 */
public class ChairSearch extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        if(request.getSession().getAttribute("validuser")==null){
                response.sendRedirect("login.jsp");
                return;
            }
        try {
            model.Chair mChair  = new model.Chair();
          if(request.getParameter("searchtype")!=null && !((String)request.getParameter("searchvalue")).equals("")){
              int type=Integer.parseInt((String)request.getParameter("searchtype"));
              String searchValue=(String)request.getParameter("searchvalue");
              javax.servlet.RequestDispatcher dispatcher=null;
              System.out.println("search type"+type);
              switch(type){
                  case 1:
                      request.setAttribute("paperinfo",mChair.getAllPaperAccordingToTitle(searchValue));
                    //  request.setAttribute("paperinfo",mChair.getAllNewPapers());
                      dispatcher = request.getRequestDispatcher("searchbychair.jsp");
                      dispatcher.forward(request, response);
                      break;
                  case 2:
                      request.setAttribute("paperinfo",mChair.getAllPaperAccordingToAuthor(searchValue));
                    //  request.setAttribute("paperinfo",mChair.getAllNewPapers());
                      dispatcher = request.getRequestDispatcher("searchbychair.jsp");
                      dispatcher.forward(request, response);
                      break;
                  case 3:
                      request.setAttribute("paperinfo",mChair.getAllPaperAccordingToReviewer(searchValue));
                    //  request.setAttribute("paperinfo",mChair.getAllNewPapers());
                      dispatcher = request.getRequestDispatcher("searchbychair.jsp");
                      dispatcher.forward(request, response);
                      break;
                  case 4:
                      request.setAttribute("paperinfo",mChair.getAllPaperAccordingToConference(searchValue));
                    //  request.setAttribute("paperinfo",mChair.getAllNewPapers());
                      dispatcher = request.getRequestDispatcher("searchbychair.jsp");
                      dispatcher.forward(request, response);
                      break;
                  case 5:
                      request.setAttribute("paperinfo",mChair.getAllPaperAccordingToPayment(searchValue));
                    //  request.setAttribute("paperinfo",mChair.getAllNewPapers());
                      System.out.println("fadfafadfafafadsfa****************88");
                      dispatcher = request.getRequestDispatcher("searchbychair.jsp");
                      dispatcher.forward(request, response);
                      break;
              }
              request.setAttribute("paperinfo",mChair.getAllNewPapers());
          }else{  
          
          request.setAttribute("paperinfo",mChair.getAllNewPapers());
          javax.servlet.RequestDispatcher dispatcher = request.getRequestDispatcher("searchbychair.jsp");
            dispatcher.forward(request, response);
         }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
