/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell
 */
public class paper extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        if(request.getSession().getAttribute("validuser")==null){
                response.sendRedirect("login.jsp");
                return;
            }
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet paper</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet paper at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
            
            //note here that we will fetch only those paper which are not in old paper state.Old paper means the papers which are
            //submitted in some previous conferences.
            model.FetchPaper fp=new model.FetchPaper();
            model.Conference conferenceModel=new model.Conference();
            java.util.List<String[]> allConferences=conferenceModel.getAllConferenceNames();
            String allPayedConferences=conferenceModel.getAllPaidConferences(Integer.parseInt(((String)(request.getSession().getAttribute("userid"))).trim()));
            String userid=(String)request.getSession().getAttribute("userid");
            if(userid==null)
                response.sendRedirect("index.jsp");
            java.util.List<String[]> list=fp.getPaperInfo(Integer.parseInt(userid));
            System.out.println(list);
            request.setAttribute("paperinfo",list);
            request.setAttribute("allconference",allConferences);
            request.setAttribute("allPaidConfIds",allPayedConferences);
            javax.servlet.RequestDispatcher dispatcher = request.getRequestDispatcher("another_page.jsp");
            dispatcher.forward(request, response);
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
