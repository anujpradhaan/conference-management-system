/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EmailFactory;
import org.json.simple.JSONObject;

/**
 *
 * @author Dell
 */
public class createNewReviewer extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String values[]=request.getParameterValues("newReviewer"); 
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet createNewReviewer</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet createNewReviewer at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
            model.Chair chairModel=new model.Chair();
            boolean flag=chairModel.createReviewerAccount(values[0],values[1]);
            JSONObject obj=new JSONObject();
            if(!flag){
                obj.put("success","success");
                model.EmailFactory emailfactory=new EmailFactory();
                model.Email email=emailfactory.getEmail("gmail");
                String body=model.DirectoryInfoModel.getNewReviewerAccountMessage();
                body+=" ----\n Your username is:"+values[0].trim()+" and your password is:"+values[1];
                new model.EmailAdapter("gmail",values[0].trim(),model.DirectoryInfoModel.chairEmailId(), "New Reviewer Account Credentials", body);
                //email.sendEmail("rambop91@gmail.com",,"New Reviewer Account Credentials",body);
            }else{
                obj.put("success","failed");
            }
            out.println(obj);
            out.flush();
            
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
