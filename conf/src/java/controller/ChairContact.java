/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EmailFactory;
import org.json.simple.JSONObject;
/**
 *
 * @author Dell
 */
public class ChairContact extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {
            model.Chair cModel=new model.Chair();
            String subject=request.getParameter("subject");
            String body=request.getParameter("body");
            String type=request.getParameter("type");
            String to="";
            if(type.equals("allreviewers")){
                to=cModel.getAllReviewersEmailIds();
            }else if(type.equals("allauthors")){
                to=cModel.getAllAuthorsEmailIds();
            }else if(type.equals("any")){
                String email=request.getParameter("email");
                to=email;
            }
            new model.EmailAdapter("gmail",to,model.DirectoryInfoModel.chairEmailId(), subject, body);
            //emailObject.sendEmail(model.DirectoryInfoModel.chairEmailId(),to, subject, body);
            JSONObject obj=new JSONObject();
                obj.put("success","success");
                out.print(obj);
                out.flush();
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChairContact</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChairContact at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
            
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
