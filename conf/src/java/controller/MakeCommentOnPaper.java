/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Email;
import model.EmailFactory;
import org.json.simple.JSONObject;
/**
 *
 * @author Dell
 */
public class MakeCommentOnPaper extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            int paperId=Integer.parseInt((String)request.getParameter("paperId").trim());
            int userId=Integer.parseInt((String)request.getParameter("userId").trim());
            String comment=((String)request.getParameter("comment"));
            model.CommonModel cModel=new model.CommonModel();
            cModel.makeComment(paperId,userId,comment);
            
            if(userId==2){
                model.EmailFactory emailfactory=new EmailFactory();
                String to =cModel.getAuthorEmailId(paperId);
                Email email=emailfactory.getEmail("GMAIL");
                // String to=(String)request.getSession().getAttribute("user_email");
                 String form=model.DirectoryInfoModel.chairEmailId();
                 String body=model.DirectoryInfoModel.ReviewCommentAddedContent();
                 new model.EmailAdapter("gmail",to,form, "Comment Added to paper", body);
                //email.sendEmail(form, to, "Comment Added to paper", body);
                
            }
            
             JSONObject obj=new JSONObject();
            obj.put("success","success");
                
                out.print(obj);
                out.flush();
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(MakeCommentOnPaper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(MakeCommentOnPaper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
