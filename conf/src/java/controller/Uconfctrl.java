/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author manish
 */
public class Uconfctrl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
         model.Conference conf = new model.Conference();
            conf.setConf_name(request.getParameter("cname"));
            conf.setCompany(request.getParameter("compname"));
            conf.setDepartment(request.getParameter("dname"));
            conf.setStart_date(request.getParameter("sdate"));
            conf.setEnd_date(request.getParameter("edate"));
            conf.setDesc(request.getParameter("desc"));
            conf.setConf_id(request.getParameter("cid"));
             String qry = "update conference set conf_name='"+conf.getConf_name()+"',department='"+conf.getDepartment()+"',start_date='"+conf.getStart_date()+"',end_date='"+conf.getEnd_date()+"',company='"+conf.getCompany()+"',description='"+conf.getDesc()+"' where conf_id="+conf.getConf_id();
            int flag= conf.createConference(qry);
           
          // out.println("flag="+flag);
           if(flag==1){
               JSONObject obj=new JSONObject();
                obj.put("success","success");
                out.print(obj);
                out.flush();
           }
           else{
               
               JSONObject obj=new JSONObject();
                obj.put("success","failure");
                out.print(obj);
                out.flush();
           
           }
        } catch (java.sql.SQLException ex) {
            Logger.getLogger(Uconfctrl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Uconfctrl.class.getName()).log(Level.SEVERE, null, ex);
        }    
         finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
