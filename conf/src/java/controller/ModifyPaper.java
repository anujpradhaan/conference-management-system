/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import org.apache.commons.io.output.*;
import org.json.simple.JSONObject;
/**
 *
 * @author Dell
 */
public class ModifyPaper extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            javax.servlet.ServletContext context = request.getServletContext();
               String filePath = context.getInitParameter("file-upload");
               String title="";
               String type="abstract";
               int maxFileSize = 5000 * 1024;
               int maxMemSize = 5000 * 1024;
               int paperId=0;
               String userid=(String)request.getSession().getAttribute("userid");
               if(userid==null)
                   response.sendRedirect("index.jsp");
               String contentType = request.getContentType();
               
               // Verify the content type
               if ((contentType.indexOf("multipart/form-data") >= 0)) {
                  
                  DiskFileItemFactory factory = new DiskFileItemFactory();
                  factory.setSizeThreshold(maxMemSize);
                  factory.setRepository(new java.io.File("C:\\temp"));
                  java.io.File file = new java.io.File(filePath+userid);
                  //out.println(filePath+userid);
                  filePath=filePath+userid+"\\";
                  boolean isFileAlreadyPresent=false;
                  if (!file.exists()) {
                    if (file.mkdir()) {
                            System.out.println("Directory is created!");
                    } else {
                            System.out.println("Failed to create directory!");
                    }
                  }else{
                      System.out.println("Directory is already present");
                  }
                  ServletFileUpload upload = new ServletFileUpload(factory);
                  upload.setSizeMax(maxFileSize);
                  
                  try{ 
                      //out.println(upload.parseRequest(request));
                    java.util.List items = upload.parseRequest(request);
                    java.util.Iterator iterator = items.iterator();
                    String fileType="";
                    while (iterator.hasNext()) 
                    {
                        FileItem item = (FileItem) iterator.next();

                        if (item.isFormField()) //your code for getting form fields
                        {
                            String name = item.getFieldName();
                            String value = item.getString();
                            if(name.equals("paperid"))
                                paperId=Integer.parseInt(value.trim());
                        }else if ( !item.isFormField () ){
                            // Get the uploaded file parameters
                            String fieldName = item.getFieldName();
                            String fileName = item.getName();
                            fileType=fileName.substring(fileName.indexOf('.')+1);
                            fileType=fileType.toLowerCase();
                            
                            java.io.File checkfile = new java.io.File(filePath+fileName);
                            //out.println(filePath+userid);
                            
                            if (!checkfile.exists()) {
                              
                            }else{
                                isFileAlreadyPresent=true;
                                JSONObject obj=new JSONObject();
                                obj.put("filestatus","exists");
                                out.println(obj);
                                out.flush();
                                System.out.println("Directory is already present");
                                return;
                           }
                        }
                    }
                  //  out.println(new Date().toString());
                    if(fileType.equals("docx")||fileType.equals("pdf")||fileType.equals("doc")){
                      if(type.equals("abstract")){
                       model.SubmitAbstract sa=new model.SubmitAbstract();
                       sa.setDirectoryStructure(filePath);
                       //sa.setStatus(0);///means not reviewd till now
                       //sa.setTitle(title);
                       //sa.setConferenceId(1);
                       //sa.setUserId(Integer.parseInt(userid));//need to get it from the session.
                       sa.setFileItems(items);//this function will upload the files
                       //sa.setPaperType(type);
                       sa.updateFileDetailsToDB(paperId);
                      }
                      else{
                       model.SubmitFinalPaper sfp=new model.SubmitFinalPaper();
                       sfp.setDirectoryStructure(filePath);
                       //sfp.setStatus(0);///means not reviewd till now
                       //sfp.setTitle(title);
                       //sfp.setConferenceId(conferenceId);
                       //sfp.setUserId(Integer.parseInt(userid));
                       sfp.setFileItems(items);
                       //sfp.setPaperType(type);
                       sfp.updateFileDetailsToDB(paperId);
                      }
                    }else{
                        out.println("Error");
                        return;
                    }
                  }catch(Exception ex) {
                     System.out.println(ex);
                  }
                  
                  //send Email to author
                  JSONObject obj=new JSONObject();
                  if(isFileAlreadyPresent)
                    obj.put("filestatus","exists");
                  else{
                    obj.put("filestatus","notexists");  
                  }          
                  obj.put("success","success");
                  out.println(obj);
                  out.flush();
                  model.EmailFactory emailfacFactory=new model.EmailFactory();
                  model.Email email= emailfacFactory.getEmail("GMAIL");
                  String to=(String)request.getSession().getAttribute("user_email");
                  String body="Please Contact admin.";
                  if(type.equals("abstract"))
                      body= model.DirectoryInfoModel.getpapersubmissiondescription();
                  else
                  body = model.DirectoryInfoModel.getabstractsubmissiondescription(); 
                  String from= model.DirectoryInfoModel.chairEmailId();
                  new model.EmailAdapter("gmail",to,model.DirectoryInfoModel.chairEmailId(), type+" Received ", body);
                  //email.sendEmail(from, to,type+" Received ", body);
               }else{
                   out.println("Error");
               }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
