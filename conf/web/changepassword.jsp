<%-- 
    Document   : changepassword
    Created on : Apr 14, 2014, 10:11:48 PM
    Author     : Dell
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="header.jsp"%>
    <div id="site_content">
      <div class="content">
        <%@include  file="chaircommonsidecontent.jsp"%>
        <h2>Change Password</h2>
        <form action="Signup" method="post" id="signup-form" class="form-style">
          <div class="form_settings">
            <p><span>Old Password</span><input type="text" name="userinfo" value="" placeholder="oldpassword" class="notnull"/><span class="error-message"></span></p>
            <p><span>New Password</span><input type="text" name="userinfo" value="" placeholder="password" class="notnull"/><span class="error-message"></span></p>
            <p><span>Confirm Password</span><input type="text" name="userinfo" value="" placeholder="password" class="notnull"/><span class="error-message"></span></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" id="submitForm" type="button" name="name" value="Update" /></p>
          </div>
        </form>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/form-validate.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>

