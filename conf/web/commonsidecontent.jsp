<%-- 
    Document   : commonsidecontent
    Created on : Apr 18, 2014, 10:17:08 PM
    Author     : ATIT
--%>
<div id="sidebar_container">
        <img class="paperclip" src="images/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Latest Conference</h3>
          <h4><span id="conferenceName"></span> is arriving. </h4>
          <h5 id="startingDate"></h5>
          <p id="conferenceDesc"><br/><a href="#" id="readMore">Read more</a></p>
        </div>
        <img class="paperclip" src="images/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Upcoming Conferences</h3>
          <ul>
            <li><a href="#">First Link</a></li>
            <li><a href="#">Another Link</a></li>
            <li><a href="#">And Another</a></li>
            <li><a href="#">Last One</a></li>
          </ul>
        </div>
</div>