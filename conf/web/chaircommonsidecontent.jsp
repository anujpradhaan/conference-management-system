<%-- 
    Document   : chaircommonsidecontent
    Created on : Apr 19, 2014, 7:36:01 PM
    Author     : Dell
--%>
<div id="sidebar_container">
        <img class="paperclip" src="images/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Latest Conference</h3>
          <h4><span id="conferenceName"></span> is arriving. </h4>
          <h5 id="startingDate"></h5>
          <p id="conferenceDesc"><br/><a href="#" id="readMore">Read more</a></p>
        </div>
        <img class="paperclip" src="images/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Useful Links</h3>
          <ul>
            <li><span class='usefullinks' id="contactAllAuthors">Contact All Author's</span></li>
            <li><span class='usefullinks' id="contactAllReviewers">Contact All Reviewer's</span></li>
            <li><span class='usefullinks' id="contactAnyIndividual">Contact Individual's</span></li>
          </ul>
        </div>
</div>
