<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
   <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script type="text/javascript" src="js/common.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $( "#tabs" ).tabs({
      collapsible: true,
      heightStyle: "content",
      hide: { effect: "explode", duration: 1000 },
      show: { effect: "blind", duration: 800 }
    });
  });
  </script>
</head>

<body>
  <div id="main">
    <%@include file="loading.jsp" %>
    <%@include  file="header.jsp"%>
    <div id="site_content">
      <%@include file="commonsidecontent.jsp" %>
        
      <div class="content">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/another_page.png" alt="another page" /><h1 style="margin: 15px 0 0 0;">Paper Information:</h1>
        <div id="tabs">
          <ul>
            <li><a href="#tabs-1">Upload Paper</a></li>
            <li><a href="#tabs-2">View Papers</a></li>
            <li><a href="#tabs-3">Modify Papers</a></li>
          </ul>
          <div id="tabs-1">
            <p><strong>Upload your paper (doc/docx/pdf format only)</strong></p>
            <form action="upload" method="post" enctype="multipart/form-data" id="fileupload-form" class="form-style">
              <div class="form_settings">
                <p><span>Select The Type:</span>
                    <select id="id" class="typeofpaper" name="typeofpaper">
                        <option value="abstract">Abstract</option>
                        <option value="finalpaper">Final Paper</option>
                    </select>
                </p>
                <p><span>Select The Conference:</span>
                    <select id="selectedconference" name="conference">
                        <%
 java.util.List<String[]> allconferences=(java.util.List<String[]>)request.getAttribute("allconference");
 java.util.Iterator<String[]> itr1=allconferences.iterator();
 while(itr1.hasNext()){
     String []conference=itr1.next();
%>
                        <option value="<%out.println(conference[0]);%>"><%out.println(conference[1]);%></option>
                        <%
}
%>
                    </select>
                </p>
                <p><span>Title :</span><input type="text" class="disabled" id="titleofpaper" name="titleofpaper" size="50" /><span class="error-message"></span></p>
                <p><span>Browse :</span><input type="file" class="disabled" name="file" size="50" /><span class="error-message"></span></p>
                <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit disabled" id="submitForm" type="button" name="name" value="Upload Paper" /></p>
              </div>
            </form>
          </div>
          <div id="tabs-2">
            <p><strong>Click On the Link Below Paper to View</strong></p>
            <table style="width:100%; border-spacing:0;">
                  <tr><th>S.No</th><th>Title</th><th></th></tr>
                  <%
                  String allConferencesForWhichAbstractIsPrestntInAString="";
    java.util.List<String[]> paperlist=(java.util.List<String[]>)request.getAttribute("paperinfo");
    java.util.Iterator<String[]> itr=paperlist.iterator();
    while(itr.hasNext()){
    String info[]=itr.next();
    %>
    <tr class="<%out.println("paper"+info[0]);%>"><td><%out.println(info[0]);%></td>
        <td>
            <a href="<% out.println(request.getContextPath()+"\\data\\"+(String)session.getAttribute("userid")+"\\"+info[7]);%>"><%out.println(info[1]);%></a></td>
        <td>
            <a class="show" data-paperId="<%out.println(info[0]);%>" data-authorId="<%out.println((String)session.getAttribute("userid"));%>" data-title="<%out.println(info[7]);%>">
                View
            </a>
        </td></tr>
                  <%
}
%>
            </table>
            <p></p>
          </div>
          <div id="tabs-3">
            <p><strong>Modification Functionality Goes Here</strong></p>
            <table style="width:100%; border-spacing:0;">
                  <tr><th>S.No</th><th>Title</th><th></th></tr>
                  <%
        itr=paperlist.iterator();
while(itr.hasNext()){
    String info[]=itr.next();
    if(allConferencesForWhichAbstractIsPrestntInAString.equals(""))
        allConferencesForWhichAbstractIsPrestntInAString=info[6];
    else
        allConferencesForWhichAbstractIsPrestntInAString+=","+info[6];
    //out.println(itr);
    %>
    <tr class="<%out.println("paper"+info[0]);%>"><td><%out.println(info[0]);%></td>
        <td>
            <a href="<% out.println(request.getContextPath()+"\\data\\"+(String)session.getAttribute("userid")+"\\");%>"><%out.println(info[1]);%></a></td>
        <td>
            <button class="withDraw" data-paperId="<%out.println(info[0]);%>">
                WithDraw
            </button>
        </td>
        <td>
            <%String decision="";
                if(Integer.parseInt(info[3])!=1)
                    decision="disabled='disabled'";
                    else
                    decision="";
                    %>
            <button class="Modify" <%out.println(decision);%> data-paperId="<%out.println(info[0]);%>">Modify Paper</button>
    
    </tr>
                  <%
}
%>
            </table>
          </div>
        </div>
      </div>
    </div>
            <input type="hidden" id="allRegisteredConferences" value="<%out.println(allConferencesForWhichAbstractIsPrestntInAString);%>" />
            <input type="hidden" id="allPayedConferences" value="<%out.println((String)request.getAttribute("allPaidConfIds"));%>"/>       
            <%@include file="commentform.jsp" %>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script src="js/jquery.form.js"></script> 
  <script src="js/uploadfile.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
