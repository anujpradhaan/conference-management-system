<%-- 
    Document   : createreviewer
    Created on : Apr 18, 2014, 2:03:22 PM
    Author     : Dell
--%>

<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
  
</head>

<body>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
      <%@include  file="chaircommonsidecontent.jsp"%>
      <div class="content">
       <p>Create New Reviewer :</p>
        <form id="createReviewer" action="#" method="post">
          <div class="form_settings">
            <p><span>Email Address</span><input class="contact" type="text" name="newReviewer" value="" /></p>
            <p><span>Password :</span><input type="password" name="newReviewer" /></p>
            <p><span>Confirm Password :</span><input type="password" name="newReviewer" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit createReviewer" type="button" name="contact_submitted" value="Create New" /></p>
          </div>
        </form>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/chair.js"></script>
  <script src="js/common.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
