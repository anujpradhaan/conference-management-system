<%-- 
    Document   : login
    Created on : Apr 14, 2014, 2:01:02 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
          <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
          <script type="text/javascript" src="js/jquery-ui.js"></script>
          <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
          <link rel="stylesheet" href="css/jquery-ui.css"></link>
          <link rel="stylesheet" href="css/style.css"></link>
          <script src="js/common.js"></script>
          <script type="text/javascript">
            $(document).ready(function(){
               $("#loading").hide();
               $("#submitLoginForm").click(function(){
                  $("#loading").show();
                  var dataString=$("#loginForm").serialize();
                  $.ajax({
                    url:'login',
                    type:'post',
                    data:dataString,
                    dataType:'json',
                    beforeSend:function(){
                    //	$("#loading").show();
                    },
                    success:function(data){
                        $("#loading").hide();
                         if(data.success=="failed"){
                             $("#loginMsg").text("Authentication Failed!!. Please try again");
                         } else{
                             $("#redirectForm").submit();
                         }
                        
                            //if(data.hasOwnProperty('message'))
                            //{
                            //	var info=data.message;
                                    //alert($.trim(info[0]['info']));
                            /*	if(data.hasOwnProperty('menge'))
                                    {
                                            var infoData=titleInfoMessage(data.message,data.menge,title);
                                            showInfoDialog(title,infoData);	
                                    }
                                    else
                                    {
                                            showInfoDialog(title,info[0]['info']);
                                    }	*/
                            //}
                    },
                    error:function(){
                    //	showError(__('Some_Error_Occured'));
                    }
                  });
               });
              $("#dialog-form").dialog({
              modal: true, 
              zIndex: 10000,
              title: "Login",
              show: { effect: "fadeIn", duration: 1200 },
              width: 'auto'
              });
          });
          </script>
    </head>
    <body>
    <%@include  file="loading.jsp"%>    
  <div id="dialog-form" title="Create new user">
      <form action="auth" method="post" id="redirectForm">
          
      </form>
      <p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>
      <form action="#" method="post" id="loginForm">
          <div class="form_settings">
            <p><span>Email :</span><input type="text" name="login" value="" /></p>
            <p><span>Password</span><input type="password" name="login" value="" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitLoginForm" name="name" value="Login" /></p>
          </div><!--
          <fieldset>
            <label for="email">Email</label>
            <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all">
          </fieldset>-->
      </form>
  </div>
  
  <div id="main">
    <%@include  file="header.jsp"%>
    <div id="site_content">
      <%@include  file="commonsidecontent.jsp"%>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      
    });
  </script>
    </body>
</html>
