<%-- 
    Document   : header
    Created on : Apr 13, 2014, 10:33:04 AM
    Author     : Dell
--%>

<header>
  <div id="logo">
    <div id="logo_text">
      <!-- class="logo_six", allows you to change the colour of the text -->
      <h1><a href="index.jsp">BITS<span class="logo_six">-Connectro</span></a></h1>
      <h2>A Conference Management System</h2>
    </div>
  </div>
  <nav>
    <div id="menu_container">
      <ul class="sf-menu" id="nav">
        <li><a href="index.jsp">Home</a></li>
        <%
        if(session.getAttribute("validuser")==null){
            
%>
        <li><a href="signup.jsp">Sign Up</a></li>
        <%
        }
        %>
        <li><a href="Pay">Payment</a></li>
        <li><a href="process">Process Flow</a></li>
        <%
        if(session.getAttribute("validuser")!=null){
        %>
        <li><a href="paper">Paper</a></li>
        <%}else{
        %>
           <!-- <li><a href="pageinfo">Paper</a></li>-->
        <%
         }
        %>
        <%
        if(session.getAttribute("validuser")!=null){
            
%>
<li><a href="#"><% out.println(session.getAttribute("validuser")); %></a>
          <ul>
            <li><a href="updateprofile">Update Profile</a></li>
            <li><a href="changepassword">Change Password</a></li>
            <li><a href="logout">Logout</a></li>
          </ul>
        </li>
         <%
            }else{
         %>
            <li><a href="login.jsp">Login</a></li>
         <%}%>
        <li><a href="contactus">Contact Us</a></li>
      </ul>
    </div>
  </nav>
</header>
