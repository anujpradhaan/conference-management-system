<%--
    Document   : chairdashboard.jsp
    Created on : Apr 16, 2014, 11:35:39 AM
    Author     : Dell
--%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

  
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
</head>

<body>
     <%@include  file="loading.jsp"%>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
      <div class="content" style="width:97%">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" />
        <h1 style="margin: 15px 0 0 0;">Welcome to Chair Dashboard</h1>
        <p>This dashboard will provide all the admin/chair operations in utmost easy way. Below Present the details of all the
           conferences. 
        </p>
        <div id="tabs">
          <ul>
            <li><a href="#tabs-1">Abstracts</a></li>
          </ul>
          <div id="tabs-1">
              <%
ArrayList<String> rids=(ArrayList<String>)request.getAttribute("rid");
ArrayList<String[]> rinfo=(ArrayList<String[]>)request.getAttribute("rinfo");

%>
            <p><strong>Abstract related Information:</strong></p>
            <div id="accordion">
                
              <% for(int k=0;k<rids.size();k++){
              String[] splitarray= (rids.get(k)).split("_");
               int rvrid=Integer.parseInt(splitarray[0]);
              String rname=splitarray[1];
                   
              %>
              <h3>Reviewed Papers by <% out.println(rname);%> </h3>
              <div>
                    <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Submission Date</th><th>Paper_status</th><th>Author name</th><th>Conference name</th><th>FilePath</th><th>Points</th><th>Type of paper</th></tr>
                      
                      <% 
                      for(int j=0;j<rinfo.size();j++){
                         String[] temp=rinfo.get(j);
                         int byreviewid=Integer.parseInt(temp[8]);
                         if(rvrid==byreviewid){
                          out.println("<tr><td>"+temp[0]+"</td>"); 
                          out.println("<td>"+temp[1]+"</td>");
                          
                          out.println("<td>"+temp[2]+"</td>");
                          out.println("<td>"+temp[3]+"</td>");
                           out.println("<td >"+temp[4]+"</td>");
                          out.println("<td> <input type=\"button\" class=\"submit ViewPaper\" data-PaperId=\""+temp[10]+"\" data-authorId=\""+temp[9]+"\" data-filename=\""+temp[5]+"\" value=\""+temp[5]+"\"/></td>");
                          out.println("<td>"+temp[6]+"</td>");
                          out.println("<td>"+temp[7]+"</td>");
                          out.println("</tr>");
                         }
                         
                     }
                     

                        %>
                                                                    
                    </table>
                
             
              </div>
              <% }%> 
            </div>
          </div> 
        </div>
       </div>
    </div>
    <div id="allAuthor-form" title="Create new user">
      <!--<p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>-->
      <form action="#" method="post" id="authorForm">
          <div class="form_settings">
            <p><span>Subject :</span><input type="text" name="subject" id="authorSubject" value="" /></p>
            <p><span>Message :</span><textarea rows="15" id="authorEmailBody"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitAllAuthorForm" name="name" value="Send Email" /></p>
          </div>
      </form>
    </div>
    <div id="allReviewer-form" title="Create new user">
      <!--<p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>-->
      <form action="#" method="post" id="reviewerForm">
          <div class="form_settings">
            <p><span>Subject :</span><input type="text" id="reviewerSubject" name="login" value="" /></p>
            <p><span>Message :</span><textarea rows="15" id="reviewerEmailBody"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitAllReviewerForm" name="name" value="Send Email" /></p>
          </div>
      </form>
    </div>
    <div id="anyIndividual-form" title="Create new user">
      <!--<p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>-->
      <form action="#" method="post" id="individualForm">
          <div class="form_settings">
            <p><span>Email :</span><input type="email" name="login" id="anyEmail" value="" /></p>  
            <p><span>Subject :</span><input type="text" id="anySubject" name="login" value="" /></p>
            <p><span>Message :</span><textarea rows="15" id="anyEmailBody"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitIndividualForm" name="name" value="Send Email" /></p>
          </div>
      </form>
    </div>
<%@include file="chaircommentform.jsp" %>                  
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/reviewer.js"></script>
 
</body>

</html>

