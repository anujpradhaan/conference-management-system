<%-- 
    Document   : chairheader
    Created on : Apr 17, 2014, 12:00:38 AM
    Author     : Dell
--%>


<header>
  <div id="logo">
    <div id="logo_text">
      <!-- class="logo_six", allows you to change the colour of the text -->
      <h1><a href="auth">BITS<span class="logo_six">-Connectro</span></a></h1>
      <h2>A Conference Management System</h2>
    </div>
  </div>
  <nav>
    <div id="menu_container">
      <ul class="sf-menu" id="nav">
        <li><a href="auth">Home</a></li>
        <li><a href="#">Manage Conferences</a>
          <ul>
            <li><a href="CreateNewConference">Create New Conference</a></li>
            <li><a href="Confdetails">View All Conference</a></li>
          </ul>    
        </li>
        <li><a href="#">Manage Reviewer</a>
          <ul>
            <li><a href="createReviewer">Create New Reviewer</a></li>
            <li><a href="modifyReviewer">Modify A Reviewer</a></li>
          </ul>    
        </li>
        <li><a href="ReviewerAllInformation">Show overview </a></li>
        <li><a href="ChairSearch">Search</a></li>
        <%
        if(session.getAttribute("validuser")!=null){
            
%>
<li><a href="#"><% out.println(session.getAttribute("validuser")); %></a>
          <ul>
            <li><a href="updateprofile">Update Profile</a></li>
            <li><a href="changepassword">Change Password</a></li>
            <li><a href="logout">Logout</a></li>
          </ul>
        </li>
         <%
            }else{
         %>
            <li><a href="login.jsp">Login</a></li>
         <%}%>
         <li><a href="#">Contact Us</a></li>
      </ul>
    </div>
  </nav>
</header>
