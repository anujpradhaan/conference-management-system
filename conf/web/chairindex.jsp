<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
        <%@include file="commonsidecontent.jsp" %>
      <div class="content">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" />
        <h1 style="margin: 15px 0 0 0;">Welcome to BITS-Connectro</h1>
        <p>BITS-Connectro is a conference management system for <a href="http://creativecommons.org/licenses/by/3.0">BITS Pilani</a> 
            that is flexible, easy to use,and has many features to make it suitable for various conference models.
        </p>
        <p>
            More than an online registration system, BITS-Connectro takes care of all your event management needs in one integrated package that includes:
        <ul>
            <li>Abstract submission and review</li>
            <li>Online registration and payment</li>
            <li>Membership solutions for associations</li>
        </ul>   
            
        </p>
        <p>
            Why use multiple systems when one will manage the whole job? Our System simplifies your event planning by including everything you need in one complete package. There is no danger of information slipping between the cracks which means less mistakes and a better experience for everyone.
            Our System is available in separate modules or as a complete package to fit the needs of a full range of conference types and sizes. From a training seminar to an international conference, <strong>BITS-Connectro</strong> offers a customized package for a stress-free event.
        </p>
        <p>
            BITS-Connectro is particularly designed to organize a conference,event in BITS Pilani. 
            To check out newer conferences click <a href="#">here.</a>
        </p>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      
    });
  </script>
  
</body>

</html>
