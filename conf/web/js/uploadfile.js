/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
      $(".show").click(function(){
    var paperTitle=$(this).attr("data-title");
    var paperId=$(this).attr("data-PaperId");
    var authorId=$(this).attr("data-authorId");
    $(".addComment").attr("data-paperId",paperId);
    var hiddenCommentBox=$(".addComment");
    hiddenCommentBox.addClass("visibleCommentButton");
   var hiddentextArea=$("#comment");
   hiddentextArea.addClass("visibleCommentBox");
   var hiddenCommentDiv=$("#commentsDiv");
   hiddenCommentDiv.addClass("visibleCommentDiv");
   
   $.ajax({
        url:'AllComments',
        type:'post',
        data:"paperid="+paperId,
        dataType:'json',
        beforeSend:function(){
            
        },
        success:function(data){
            $('<div></div>').appendTo('body')
            .html("<div id='detailID' style='overflow:scroll;height:800px'>"+
                "<table width='100%' style='background-color:#fff;' height=100%><tr><td width='70%' style='background-color:#fff;'>"
                +"<iframe  style='width:100%;height:100%;' src='data/"+authorId+"/"+paperTitle+"'></iframe>"+
                "</td><td style='background-color:#fff;'>"+
                "<div>"+$("#commentform").html()+
                "</div></td></tr></table>"+
                +"</div>")
            .dialog({ 
              width: 1250, 
              height: 600, 
              left:50,
              position: 'left', 
              resizable: true,
              model:true,
              zIndex:10000

          });
  //("#sidebar_container1").remove();
              hiddenCommentBox.removeClass("visibleCommentButton");
              hiddentextArea.removeClass("visibleCommentBox");
              hiddenCommentDiv.removeClass("visibleCommentDiv");
         var cmnt="";
          var i=0;
            $.each(data,function(index,value){
                if(index!="no" && index!="success"){
                     var values=value.split(",");
                    cmnt+="<h5 class='nopadding'> "+((values[0]=="2"||values[0]==2)?"Reviewer":"Author")+" said :</h5>"+ "<p class='nopadding' style='margin-left: 30px;'>"+values[2]+"<hr>";  
                }
            });
            $(".visibleCommentDiv").append(cmnt);          
        },
        error:function(){
        alert('error');
        }
  });
      });
    $("#submitForm").click(function(){
          //alert("dsfsdfdsddsf="+$("#fileupload-form").serialize());
          var flag=false;
          if($("#titleofpaper").val()==""){
              alert("Please give a title to this paper.")
              flag=true;
              return false;
          }
              
          
          if($(".typeofpaper").val()=="abstract"){
               var selectedConf=$("#selectedconference").val(); 
               var paidConf=$("#allPayedConferences").val().split(",");
               flag=false;
               //alert(selectedConf);
               $.each(paidConf,function(index,value){
                   //alert(value);
                   if(parseInt(selectedConf,10)==parseInt(value,10)){
                       flag=true;
                     //  alert("paid");
                   }
               });
               if(flag==true){
                   alert("You have already Submitted An Abstract to this paper");
                   return false;
               }
          }
          if(flag==false){
          var options = { 
                    beforeSubmit:  showRequest,  // pre-submit callback 
                    success:       showResponse,  // post-submit callback 
                    dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
                    error:showError
                };     
          
        $('#fileupload-form').ajaxForm(options).submit(); 
        }
      });
      
       $(".Modify").click(function(){
        var paperId=$(this).attr("data-PaperId");
        modifyPaperDialog(paperId);
    });
    
    function modifyPaperDialog(paperId){
        $('<div></div>').appendTo('body')
        .html("<div>"+("Do you really want to Modify?")+"</div>"+
            "<form action=\"ModifyPaper\" method=\"post\" enctype=\"multipart/form-data\" id=\"modifyupload-form\" class=\"form-style\">"+
              "<div class=\"form_settings\"><input type=\"file\" name=\"file\" size=\"30\" />"+
                "<input type='hidden' name='paperid' value='"+paperId+"'/></div></form>")
        .dialog({
        modal: true, 
        title: ('Do you Really Want To Modify'),
        zIndex: 10000,
        autoOpen: true,
        width: 'auto', 
        buttons:{
        Ok : function (){	
                /*$.ajax({
                    url:'ModifyPaper',
                    type:'post',
                    data:"paperid="+paperId,
                    dataType:'json',
                    beforeSend:function(){
                            
                    },
                    success:function(data){
                        
                    },
                    error:function(){

                    }
                });*/
                //$(".paper"+paperId).remove();
                var options = { 
                    beforeSubmit:  showRequest,  // pre-submit callback 
                    success:       showResponse,  // post-submit callback 
                    dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
                    error:showError
                }; 
 
                $('#modifyupload-form').ajaxForm(options).submit(); 
                $(this).remove();
            },
         No:function(){
             $(this).remove();
         }   
        },
        close: function (event, ui){
            $(this).remove();
           }
        });
    }
    /*Below Are the functions for ajax form submit*/
      // pre-submit callback 
        function showRequest(formData, jqForm, options) { 
            var queryString = $.param(formData); 
                //alert('About to submit: \n\n' + queryString); 
                $("#loadingMsg").text("Uploading the paper...");
                $("#loading").show();
                return true; 
        } 

        // post-submit callback 
        function showResponse(data, statusText, xhr, $form)  { 
            // if the ajaxForm method was passed an Options Object with the dataType 
            // property set to 'json' then the first argument to the success callback 
            // is the json data object returned by the server 
            $("#loading").hide();
            $("#loadingMsg").text("Loading..");
            if(data.filestatus=="exists")
                alert("This paper already exits");
            else{
                alert("Paper Update successfull");
                window.location.reload();
            }
        }
        function showError()  { 
            $("#loading").hide();
            $("#loadingMsg").text("Loading...");
            alert("Upload Error.Ensure you are uploading only pdf/doc/docx format.")
        }
    /*Ajax form function ends here*/  
      
      $(".withDraw").click(function(){
        var paperId=$(this).attr("data-PaperId");
        rejectPaperDialog(paperId);
    });
    
    function rejectPaperDialog(paperId){
        $('<div></div>').appendTo('body')
        .html("<div>"+("Do you really want to Delete?")+"</div>")
        .dialog({
        modal: true, 
        title: ('Do you Really Want To WithDraw'),
        zIndex: 10000,
        autoOpen: true,
        width: 'auto', 
        buttons:{
        Ok : function (){	
                $.ajax({
                    url:'RejectPaper',
                    type:'post',
                    data:"paperid="+paperId,
                    dataType:'json',
                    beforeSend:function(){
                            $("#loading").show();
                            $("#loadingMsg").text("Loading...");
                    },
                    success:function(data){
                        $("#loading").hide();
                    },
                    error:function(){
                        $("#loading").hide();
                    }
                });
                $(".paper"+paperId).remove();
                $(this).remove();
            },
         No:function(){
             $(this).remove();
         }   
        },
        close: function (event, ui){
            $(this).remove();
           }
        });
    }
    
   $(document).on('click','.visibleCommentButton', function(){
        if($(".visibleCommentBox").val()==""){
            alert("Null value");
            $(".visibleCommentBox").focus();
        }else{
            alert("making comment");
            var paperId=$(".visibleCommentButton").attr("data-paperid");
            var userId=$(".visibleCommentButton").attr("data-userid");
            var comment=$(".visibleCommentBox").val();
            var dataString="paperId="+paperId+"&userId="+userId+"&comment="+comment;
            alert(dataString);
            $.ajax({
                url:'MakeCommentOnPaper',
                type:'post',
                data:dataString,
                dataType:'json',
                beforeSend:function(){
                    $("#loading").show();
                },
                success:function(data){
                    $("#loading").hide();
                    var comment1="<h5 class='nopadding'>Person 1 said: </h5>"+
                                  "<p class='nopadding'>"+comment+"<br /></p>"+
                                  "<hr>";
                    $(".visibleCommentDiv").append(comment1);          
                },
                error:function(){
                    $("#loading").hide();
                    alert("Something Wrong Occured!!");
                }
                });
        }
    });
    
    $(".typeofpaper,#selectedconference").on("change",function(){
        if($(".typeofpaper").val()=="finalpaper"){
       // alert("checking");
       var selectedConf=$("#selectedconference").val(); 
       var paidConf=$("#allPayedConferences").val().split(",");
       var flag=false;
       //alert(selectedConf);
       $.each(paidConf,function(index,value){
           //alert(value);
           if(parseInt(selectedConf,10)==parseInt(value,10)){
               flag=true;
             //  alert("paid");
           }
       });
       
  // return false;
       if(flag==true){
           $(".typeofpaper").val("finalpaper");
           $(".disabled").removeAttr('disabled');
       }
       else {
           $(".typeofpaper").val("abstract");
           alert("You have to first make the payment. Then Only you can submit this paper");
           $(".disabled").prop('disabled', true);
           $("#selectedconference").val("");
       }
     }else{
         $(".disabled").removeAttr('disabled');
     }
    });
});


