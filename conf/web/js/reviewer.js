/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
      $("#loading").hide();  
      $('ul.sf-menu').sooperfish();
      $( "#tabs" ).tabs({
      collapsible: true,
      heightStyle: "content",
      hide: {effect: "blind", duration: 1000},
      show: {effect: "blind", duration: 800}
      });
      $( "#accordion" ).accordion({
      event: "click hoverintent",
      heightStyle: "content"
    });
    $( "#accordion1" ).accordion({
      event: "click hoverintent",
      heightStyle: "content"
    })
    
    /*Code for contact dialof boxes goes here*/
    $("#allAuthor-form").hide();
    $("#allReviewer-form").hide();
    $("#anyIndividual-form").hide();
    $("#contactAllAuthors").click(function(){
        $("#allAuthor-form").dialog({
            modal: true, 
            zIndex: 10000,
            title: "Send Email To all The Author's",
            show: {effect: "fadeIn", duration: 1200},
            width: 'auto'
        });
    });
    $("#contactAllReviewers").click(function(){
        $("#allReviewer-form").dialog({
            modal: true, 
            zIndex: 10000,
            title: "Send Email To all The Reviewer's",
            show: {effect: "fadeIn", duration: 1200},
            width: 'auto'
        });
    });
    $("#contactAnyIndividual").click(function(){
        $("#anyIndividual-form").dialog({
            modal: true, 
            zIndex: 10000,
            title: "Send Email To Any One",
            show: {effect: "fadeIn", duration: 1200},
            width: 'auto'
        });
    });
    $(".requestToReassign").click(function(){
        var paperId=$(this).attr("data-paperId");
        var form="<form ><p><span>Reason</span><textarea class='contact textarea whiteBackground' id='reasoncomment' rows='4' cols='40' name='reason'></textarea></p></form>";
        $('<div></div>').appendTo('body')
        .html("<div>"+form+"</div>")
        .dialog({
            modal: true, 
            title: ('Reassign'),
            zIndex: 10000,
            autoOpen: true,
            width: 'auto', 
            buttons:{
                Submit : function ()
                {	
                    $.ajax({
                        url:'RequestReassign',
                        type:'post',
                        data:"paperid="+paperId+"&reason="+($("#reasoncomment").val()!=''?$("#reasoncomment").val():"No reason given"),
                        dataType:'json',
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(data){
                          //  $("#loadingMsg").text("Almost Done");
                           $("#loading").hide();
                          alert("done");
                          $(".paper"+paperId).remove();
                        },
                        error:function(){

                        }
                    });
                    $(this).remove();
                },
                Cancel : function ()
                {	
                    $(this).remove();
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
    });
    /*
     *View The paper
     **/
    $(".show").click(function(){
    var paperTitle=$(this).attr("data-filename");
    var paperId=$(this).attr("data-PaperId");
    
    var authorId=$(this).attr("data-authorId");
    $(".addComment").attr("data-paperId",paperId);
    var hiddenCommentBox=$(".addComment");
    hiddenCommentBox.addClass("visibleCommentButton");
   var hiddentextArea=$("#comment");
   hiddentextArea.addClass("visibleCommentBox");
   var hiddenCommentDiv=$("#commentsDiv");
   hiddenCommentDiv.addClass("visibleCommentDiv");
   
   $.ajax({
        url:'AllComments',
        type:'post',
        data:"paperid="+paperId,
        dataType:'json',
        beforeSend:function(){
            
        },
        success:function(data){
            $('<div></div>').appendTo('body')
            .html("<div id='detailID' style='overflow:scroll;height:800px'>"+
                "<table width='100%' style='background-color:#fff;' height=100%><tr><td width='70%' style='background-color:#fff;'>"
                +"<iframe  style='width:100%;height:100%;' src='data/"+authorId+"/"+paperTitle+"'></iframe>"+
                "</td><td style='background-color:#fff;'>"+
                "<div>"+$("#commentform").html()+
                "</div></td></tr></table>"+
                +"</div>")
            .dialog({ 
              width: 1250, 
              height: 600, 
              left:50,
              position: 'left', 
              resizable: true,
              model:true,
              zIndex:10000

          });
  //("#sidebar_container1").remove();
              hiddenCommentBox.removeClass("visibleCommentButton");
              hiddentextArea.removeClass("visibleCommentBox");
              hiddenCommentDiv.removeClass("visibleCommentDiv");
         var cmnt="";
          var i=0;
            $.each(data,function(index,value){
                if(index!="no" && index!="success"){
                     var values=value.split(",");
                    cmnt+="<h5 class='nopadding'> "+((values[0]=="2"||values[0]==2)?"Reviewer":"Author")+" said :</h5>"+ "<p class='nopadding' style='margin-left: 30px;'>"+values[2]+"<hr>";  
                }
            });
            $(".visibleCommentDiv").append(cmnt);          
        },
        error:function(){
        alert('error');
        }
     });
    
  });
  
  /*
   *Make Comments 
   **/
  $(document).on('click','.visibleCommentButton', function(){
        if($(".visibleCommentBox").val()==""){
            alert("Null value");
            $(".visibleCommentBox").focus();
        }else{
            alert("making comment");
            var paperId=$(".visibleCommentButton").attr("data-paperid");
            var userId=$(".visibleCommentButton").attr("data-userid");
            var comment=$(".visibleCommentBox").val();
            var dataString="paperId="+paperId+"&userId="+userId+"&comment="+comment;
           // alert(dataString);
            $.ajax({
                url:'MakeCommentOnPaper',
                type:'post',
                data:dataString,
                dataType:'json',
                beforeSend:function(){
                    $("#loadingMsg").text("Putting the Comment");
                    $("#loading").show();
                    
                },
                success:function(data){
                    $("#loadingMsg").text("Almost Done");
                    $("#loading").hide();
                    var cmnt="<h5 class='nopadding'>"+("Person")+" said :</h5>"+ "<p class='nopadding' style='margin-left: 30px;'>"+comment+"<hr>";  
                    $(".visibleCommentDiv").append(cmnt);
                    $("#loadingMsg").text("Loading...");
                },
                error:function(){
                    $("#loadingMsg").text("Loading...");
                    alert('error');
                }
           });
        }
    });
    
    $(".marks").click(function(){
        var paperId=$(this).attr("data-paperId");
        var form="<form><p> Give marks in 10 Scale.</p><br/> <p><span>Marks </span><select id='mark'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option></select></p></form>";
        $('<div></div>').appendTo('body')
        .html("<div>"+form+"</div>")
        .dialog({
            modal: true, 
            title: ('Evalution'),
            zIndex: 10000,
            autoOpen: true,
            width: 'auto', 
            buttons:{
                Submit : function ()
                {	
                    var marks=$("#mark").val();
                   
                    $.ajax({
                        url:'Evalution',
                        type:'post',
                        data:"paperid="+paperId+"&marks="+marks,
                        dataType:'json',
                        beforeSend:function(){
                           
                        },
                        success:function(data){
                          //  $("#loadingMsg").text("Almost Done");
                          alert("done");
                         window.location.reload(true);
                        },
                        error:function(){
                               alert('error');
                        }
                    });
                    $(this).remove();
                },
                Cancel : function ()
                {	
                    $(this).remove();
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
    });
    function hide(){
        $("#loading").hide();
    };
    $(".ViewPaper").click(function(){
    $("#loading").show();
    var paperTitle=$(this).attr("data-filename");
    var paperId=$(this).attr("data-PaperId");
    //alert(paperId);
    var authorId=$(this).attr("data-authorId");
    $(".addComment").attr("data-paperId",paperId);
    var hiddenCommentBox=$(".addComment");
    hiddenCommentBox.addClass("visibleCommentButton");
   var hiddentextArea=$("#comment");
   hiddentextArea.addClass("visibleCommentBox");
   var hiddenCommentDiv=$("#commentsDiv");
   hiddenCommentDiv.addClass("visibleCommentDiv");
   
   $.ajax({
        url:'AllComments',
        type:'post',
        data:"paperid="+paperId,
        dataType:'json',
        beforeSend:function(){
            
        },
        success:function(data){
            $('<div></div>').appendTo('body')
            .html("<div id='detailID' style='overflow:scroll;height:800px'>"+
                "<table width='100%' style='background-color:#fff;' height=100%><tr><td width='70%' style='background-color:#fff;'>"
                +"<iframe id=\"iframe\"  style='width:100%;height:100%;' src='data/"+authorId+"/"+paperTitle+"'></iframe>"+
                "</td><td style='background-color:#fff;'>"+
                "<div>"+$("#commentform").html()+
                "</div></td></tr></table>"+
                +"</div>")
            .dialog({ 
              width: 1250, 
              height: 600, 
              left:50,
              position: 'centre', 
              resizable: true,
              model:true,
              zIndex:10000

          });
  //("#sidebar_container1").remove();
              hiddenCommentBox.removeClass("visibleCommentButton");
              hiddentextArea.removeClass("visibleCommentBox");
              hiddenCommentDiv.removeClass("visibleCommentDiv");
         var cmnt="";
          var i=0;
            $.each(data,function(index,value){
                //alert(value);
                if(index!="no" && index!="success"){
                     var values=value.split(",");
                    cmnt+="<h5 class='nopadding'> "+((values[0]=="2"||values[0]==2)?"Reviewer":"Author")+" said :</h5>"+ "<p class='nopadding' style='margin-left: 30px;'>"+values[2]+"<hr>";  
                }
            });
            $(".visibleCommentDiv").append(cmnt);          
            $("#loading").hide();
        },
        error:function(){
            alert('error');
        }
     });
    
  });
  
    
    
 });
   