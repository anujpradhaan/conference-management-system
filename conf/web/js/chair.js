/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
      $("#loading").hide();  
      $('ul.sf-menu').sooperfish();
      $( "#tabs" ).tabs({
      collapsible: true,
      heightStyle: "content",
      hide: {effect: "blind", duration: 1000},
      show: {effect: "blind", duration: 800}
      });
      $( "#accordion" ).accordion({
      heightStyle: "content"
    });
    $( "#accordion1" ).accordion({
      heightStyle: "content"
    });
    
    /*Code for contact dialof boxes goes here*/
    $("#allAuthor-form").hide();
    $("#allReviewer-form").hide();
    $("#anyIndividual-form").hide();
    $("#contactAllAuthors").click(function(){
        $("#allAuthor-form").dialog({
            modal: true, 
            zIndex: 10000,
            title: "Send Email To all The Author's",
            show: {effect: "fadeIn", duration: 1200},
            width: 'auto'
        });
    });
    $("#contactAllReviewers").click(function(){
        $("#allReviewer-form").dialog({
            modal: true, 
            zIndex: 10000,
            title: "Send Email To all The Reviewer's",
            show: {effect: "fadeIn", duration: 1200},
            width: 'auto'
        });
    });
    $("#contactAnyIndividual").click(function(){
        $("#anyIndividual-form").dialog({
            modal: true, 
            zIndex: 10000,
            title: "Send Email To Any One",
            show: {effect: "fadeIn", duration: 1200},
            width: 'auto'
        });
    });
// });
 
 /*Code for ajax requests to send mails goes here*/
 $("#submitAllAuthorForm").click(function(){
        var sub=$("#authorSubject").val();
        var body=$("#authorEmailBody").val();
        if(sub=="" || body=="")
            alert("Please Enter proper values then send the mails");
        else
            sendEmails("allauthors",sub,body,"");
    });
    $("#submitAllReviewerForm").click(function(){
        var sub=$("#reviewerSubject").val();
        var body=$("#reviewerEmailBody").val();
        if(sub=="" || body=="")
            alert("Please Enter proper values then send the mails");
        else
            sendEmails("allreviewers",sub,body,"");
    });
    $("#submitIndividualForm").click(function(){
        var sub=$("#anySubject").val();
        var body=$("#anyEmailBody").val();
        var to=$("#anyEmail").val();
        if(sub=="" || body=="" || to=="")
            alert("Please Enter proper values then send the mails");
        else
            sendEmails("any",sub,body,to);
    });
    
    function sendEmails(type,subject,body,to){
        var dataString="type="+type+"&subject="+subject+"&body="+body+"&email="+to;
        $.ajax({
            url:'ChairContact',
            type:'post',
            data:dataString,
            dataType:'json',
            beforeSend:function(){
                $("#loading").show();
                $("#loadingMsg").text("Wait While I am sending the Emails...");
            },
            success:function(data){
                $("#loadingMsg").text("Almost Done...");
                $("#loading").hide();
                if(data.success=="success")
                    alert("Email sent");
                else
                    alert("Something Went Wrong!! Try Again");
                $("#loadingMsg").text("Loading...");
            },
            error:function(){
                $("#loading").hide();
                alert("Something is not Right. Error!!!")
            }
        });
    }
/*Contact ends here*/

/*Assign to reviewer and reject Logic goes here*/
///On click a popup will be displayed which will contain all the reviewer's
    
    var paperId;
    $(".assignToReviewer").on("click",function(){
        paperId=$(this).attr("data-PaperId");
        alert(paperId);
        getAllReviewers();
    });
    
    function getAllReviewers(){
        $.ajax({
            url:'getAllReviewers',
            type:'post',
            data:"",
            dataType:'json',
            beforeSend:function(){
                $("#loading").show();
            },
            success:function(data){
             var userinfo=null;
             $.each(data,function(index,value){
                if(userinfo==null)
                    userinfo="<tr><td>"+(value.userid)+"</td><td>"+(value.name)+"</td><td><input type='radio' data-userId='"+(value.userid)+"' name='selectedReviewer' class='selectedReviewer'/></td></tr>";
                else
                    userinfo+="<tr><td>"+(value.userid)+"</td><td>"+(value.name)+"</td><td><input type='radio' data-userId='"+(value.userid)+"' name='selectedReviewer' class='selectedReviewer'/></td></tr>";
             });
             userinfo="<table style='width:100%; border-spacing:0;'><tr><th>UserId</th><th>Name Of Reviewer</th><th>Select using below</th></tr>"+userinfo+"</table>";
             popTheReviewerInfo(userinfo);
             //$("#loading").hide();
            },
            error:function(){
            }
        });
    }
    function popTheReviewerInfo(userinfo){
    $("#loading").hide();
    $('<div></div>').appendTo('body')
		.html("<div>"+(userinfo)+"</div>")
		.dialog({
                    modal: true, 
                    title: ('Select A Reviewer'),
                    zIndex: 10000,
                    autoOpen: true,
                    width: 'auto', 
                    buttons:{
                        Select : function ()
                        {	
                            
                            //append particular reviewerID and make a ajax request to send the required Emails.
                            var reviewerId;
                            $(".selectedReviewer").each(function(index,value){
                               if($(this).is(':checked')){
                                   alert($(this).attr("data-userId"));
                                   reviewerId=$(this).attr("data-userId");
                               } 
                            });
                            $("#loadingMsg").text("Sending Email's");
                            assignReviewerAndSendEmail(paperId,reviewerId);
                            $(this).remove();
                        }
                    },
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
                
    }
    
    function assignReviewerAndSendEmail(paperId,reviewerId){
        $.ajax({
            url:'AssignReviewer',
            type:'post',
            data:"paperid="+paperId+"&reviewerid="+reviewerId,
            dataType:'json',
            beforeSend:function(){
                $("#loadingMsg").text("Assigning And Sending Emails");
                $("#loading").show();
                
            },
            success:function(data){
                $("#loadingMsg").text("Almost Done");
                $("#loading").hide();
                window.location.reload();
            },
            error:function(){
                alert("error");
                $("loading").hide();
            }
        });
    }
    
    $(".rejectPaper").click(function(){
        var paperId=$(this).attr("data-PaperId");
        rejectPaperDialog(paperId);
    });
    
    function rejectPaperDialog(paperId){
        $('<div></div>').appendTo('body')
        .html("<div>"+("Do you want to reject")+"</div>")
        .dialog({
        modal: true, 
        title: ('Take Decision'),
        zIndex: 10000,
        autoOpen: true,
        width: 'auto', 
        buttons:{
        Ok : function (){	
                $.ajax({
                    url:'RejectPaper',
                    type:'post',
                    data:"paperid="+paperId,
                    dataType:'json',
                    beforeSend:function(){
                        $("#loadingMsg").text("Wait till Rejecting and informing the Author");
                        $("#loading").show();     
                    },
                    success:function(data){
                        $("#loadingMsg").text("Loading...");
                        $("#loading").hide();
                    },
                    error:function(){
                        alert("Error");
                        $("#loading").hide();
                    }
                });
                $(this).remove();
            },
         No:function(){
             $(this).remove();
         }   
        },
        close: function (event, ui){
            $(this).remove();
           }
        });
    }
     $(".acceptPaper").click(function(){
        var paperId=$(this).attr("data-PaperId");
        acceptPaperDialog(paperId);
    });
    
    function acceptPaperDialog(paperId){
        $('<div></div>').appendTo('body')
        .html("<div>"+("Do you want to accept")+"</div>")
        .dialog({
        modal: true, 
        title: ('Take Decision'),
        zIndex: 10000,
        autoOpen: true,
        width: 'auto', 
        buttons:{
        Ok : function (){	
                $.ajax({
                    url:'AcceptPaper',
                    type:'post',
                    data:"paperid="+paperId,
                    dataType:'json',
                    beforeSend:function(){
                            
                    },
                    success:function(data){
                        
                    },
                    error:function(){

                    }
                });
                $(this).remove();
            },
         No:function(){
             $(this).remove();
         }   
        },
        close: function (event, ui){
            $(this).remove();
           }
        });
    }
    $(".createReviewer").click(function(){
        //validateion here
        var dataString=$("#createReviewer").serialize();
        $.ajax({
            url:'createNewReviewer',
            type:'post',
            data:dataString,
            dataType:'json',
            beforeSend:function(){
                
            },
            success:function(data){
                if(data.success=="success")
                    alert("Reviewer Created");
                else
                    alert("Something Went Wrong!! Try Again");
            },
            error:function(){

            }
        });
    });
});    
    
    
/*Assign and reject ends here*/
/* 
$(function() {
    
  });
 */
  /*
   * hoverIntent | Copyright 2011 Brian Cherne
   * http://cherne.net/brian/resources/jquery.hoverIntent.html
   * modified by the jQuery UI team
   */
  $.event.special.hoverintent = {
    setup: function() {
      $( this ).bind( "mouseover", jQuery.event.special.hoverintent.handler );
    },
    teardown: function() {
      $( this ).unbind( "mouseover", jQuery.event.special.hoverintent.handler );
    },
    handler: function( event ) {
      var currentX, currentY, timeout,
        args = arguments,
        target = $( event.target ),
        previousX = event.pageX,
        previousY = event.pageY;
 
      function track( event ) {
        currentX = event.pageX;
        currentY = event.pageY;
      };
 
      function clear() {
        target
          .unbind( "mousemove", track )
          .unbind( "mouseout", clear );
        clearTimeout( timeout );
      }
 
      function handler() {
        var prop,
          orig = event;
 
        if ( ( Math.abs( previousX - currentX ) +
            Math.abs( previousY - currentY ) ) < 7 ) {
          clear();
 
          event = $.Event( "hoverintent" );
          for ( prop in orig ) {
            if ( !( prop in event ) ) {
              event[ prop ] = orig[ prop ];
            }
          }
          // Prevent accessing the original event since the new event
          // is fired asynchronously and the old event is no longer
          // usable (#6028)
          delete event.originalEvent;
 
          target.trigger( event );
        } else {
          previousX = currentX;
          previousY = currentY;
          timeout = setTimeout( handler, 100 );
        }
      }
 
      timeout = setTimeout( handler, 100 );
      target.bind({
        mousemove: track,
        mouseout: clear
      });
    }
  };

