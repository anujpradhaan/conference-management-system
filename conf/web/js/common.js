/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $.ajax({
        url: 'LatestConference',
        type: 'post',
        data: "paperid=",
        dataType: 'json',
        beforeSend: function() {
            $("#loading").show();
            $("#loadingMsg").text("Wait. Fetchinf the New Conference Details.");
        },
        success: function(data) {
            $("#loading").hide();
            $("#startingDate").text(data.startdate);
            $("#conferenceName").text(data.conferenceName);
            $("#conferenceDesc").text(data.description);
            $("#readMore").attr("data-description",data.description);
        },
        error: function() {
            $("#loading").hide();
            alert('error');
        }
    });
    $("#readMore").click(function(){
       event.preventDefault();
       //open a popup
    });
});


