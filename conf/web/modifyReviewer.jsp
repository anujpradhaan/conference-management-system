<%-- 
    Document   : modifyReviewer
    Created on : Apr 20, 2014, 7:17:40 PM
    Author     : Dell
--%>
<!DOCTYPE HTML>
<html>
<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
        <div class="content" style="width:100%">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" />

        <table style="width:100%; border-spacing:0;">
            <tr><th>Reviewer Name</th><th>Delete</th></tr>
            <%
java.util.ArrayList<String[]> list=(java.util.ArrayList<String[]>)request.getAttribute("reviewerinfo");            
java.util.Iterator<String[]> itr=list.iterator();
while(itr.hasNext()){
    
String[] reviewerinfo=itr.next();
%>
            <tr>
                <td><%out.println(reviewerinfo[0]);%></td>
                <td><a href="<%out.println("modifyReviewer?id="+reviewerinfo[1]);%>"><input type="button" onclick="return confirm('Are you sure want to delete?')" value="delete This Reviewer" class="submit"/></a></td>
            </tr>
            <%
}
            %>
        </table>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      
    });
  </script>
  
</body>

</html>



