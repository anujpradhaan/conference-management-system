<%-- 
    Document   : updateprofile
    Created on : Apr 14, 2014, 10:02:24 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>BITS Conference</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="header.jsp"%>
    <div id="site_content">
        <%@include  file="commonsidecontent.jsp"%>
        <div class="content">
          <%
String userinfo[]=(String[])request.getAttribute("userinfo");
%>
        <h2>Update Profile </h2>
        <form action="updateProfileInfo" method="post" id="update-form" class="form-style">
          <div class="form_settings">
            <p><span>First Name</span><input type="text" name="userinfo" value="<%out.println(userinfo[1]);%>" placeholder="Firstname" class="notnull"/><span class="error-message"></span></p>
            <p><span>Last Name</span><input type="text" name="userinfo" value="<%out.println(userinfo[2]);%>" placeholder="Lastname" class="notnull"/></p>
            <p><span>Email</span><input type="email" name="userinfo" value="<%out.println(userinfo[3]);%>" placeholder="abc@xyz.com" class="notnull"/></p>
            <p><span>Date Of Birth</span><input type="date" name="userinfo" value="" class="notnull"/></p>
            <p><span>Sex</span>
                Male :<input type="radio" name="sex" value="" class=""/>
                Female:<input type="radio" name="sex" value="" class=""/>
            </p>
            <p><span>Area Of Interests</span><textarea rows="8" cols="50" name="userinfo" class="notnull"><%out.println(userinfo[5]);%></textarea></p>
            <p><span>Institute/Company</span><input type="text" name="userinfo" value="<%out.println(userinfo[6]);%>" class="notnull"/></p>
            <!--<p><span>Checkbox example</span><input class="checkbox" type="checkbox" name="name" value="" /></p>
            <p><span>Dropdown list example</span><select id="id" name="name"><option value="1">Example 1</option><option value="2">Example 2</option></select></p>
            -->
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" id="updateForm" type="button" name="name" value="Update" /></p>
          </div>
        </form>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/form-validate.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>

