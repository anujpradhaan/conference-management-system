<%-- 
    Document   : signup
    Created on : Apr 13, 2014, 10:15:09 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>BITS Conference</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
   <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
      <%@include  file="chaircommonsidecontent.jsp"%>
      <div class="content">
        <h2>Creating Conference</h2>
        <span id="conferror" class="error-message"></span>
        <form action="authupdate" method="post" id="redirectConfForm">
          
      </form>
        <form action="CreateConferece" method="post" id="conf-form" class="form-style">
          <div class="form_settings">
              
            <p><span>Conference Name</span><input type="text" name="cname" value="" placeholder="Conference name" class=""/><span class="error-message"></span></p>
            <p><span>Organizing Department</span><input type="text" name="dname" value="" placeholder="Department name" class=""/></p>
            <p><span>Organizing Company</span><input type="text" name="compname" value="" placeholder="Company name" class=""/></p>
            <p><span>Header Content:</span><textarea rows="8" cols="50" name="headercontent" class=""></textarea></p>
            <p><span>Start Date</span><input type="date" name="sdate" value="" class=""/></p>
            <p><span>End Date</span><input type="date" name="edate" value="" class=""/></p>
            <p><span>Abstract DeadLine:</span><input type="date" name="adeadline" value="" class=""/></p>
            <p><span>Paper DeadLine:</span><input type="date" name="pdeadline" value="" class=""/></p>
            <p><span>Description</span><textarea rows="8" cols="50" name="desc" class=""></textarea></p>
            <p><span>Research Topics</span><textarea rows="8" cols="50" name="topics" class=""></textarea></p>
            <p><span>Sponsers</span><textarea rows="8" cols="50" name="sponsers" class=""></textarea></p>
            <p><span>Sponsers Related Info:</span><textarea rows="8" cols="50" name="sponsersinfo" class=""></textarea></p>
            <p><span>List Of Journals:</span><textarea rows="8" cols="50" name="journals" class=""></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" id="submitConference" type="button" name="name" value="Create" /></p>
          </div>
        </form>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/form-validate-conf.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
