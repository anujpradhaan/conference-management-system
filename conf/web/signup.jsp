<%-- 
    Document   : signup
    Created on : Apr 13, 2014, 10:15:09 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>BITS Conference</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
   <script type="text/javascript" src="js/jquery.js"></script>
  <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="header.jsp"%>
    <div id="site_content">
      <%@include  file="commonsidecontent.jsp"%>
      <div class="content">
          <form action="authsignup" method="post" id="redirectForm">
          
      </form>
        <h2>Sign Up</h2>
        <span><p id="loginMsg"></p></span>
        <form action="Signup" method="post" id="signup-form" class="form-style">
          <div class="form_settings">
              <p><span>First Name</span><input type="text" name="fname" id="fname" value="" placeholder="Firstname" class="formbg1" /><span class="error-message"></span></p>
            <p><span>Last Name</span><input type="text" name="lname" value="" placeholder="Lastname" class=""/></p>
            <p><span>Email</span><input type="email" name="email" value="" placeholder="abc@xyz.com" class=""/></p>
            <p><span>Username:</span><input type="text" name="username" id="username" value="" placeholder="Username" class=""/><span class="error-message" id="usercheck"></span></p>
               
            <p><span>Password</span><input type="password" name="password" value="" placeholder="Password" class=" validatePassword"/></p>
            <p><span>Confirm Password</span><input type="password" name="cpassword" value="" placeholder="Confirm Password" class=" validatePassword"/></p>
            <p><span>Date Of Birth</span><input type="date" name="dob" value="" class=""/></p>
            <p><span>Sex</span>
                <input type="radio" name="s" value="M" class="" >Male </input>
                <input type="radio" name="s" value="F" class="" >Female </input>
            </p>
            <p><span>Institute/Company</span><input type="text" name="inst" value="" class=""/></p>
            <p><span>Area Of Interests</span><textarea rows="8" cols="50" name="interest" class=""></textarea></p>
            
            <!--<p><span>Checkbox example</span><input class="checkbox" type="checkbox" name="name" value="" /></p>
            <p><span>Dropdown list example</span><select id="id" name="name"><option value="1">Example 1</option><option value="2">Example 2</option></select></p>
            -->
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" id="submitForm" type="button" name="name" value="Signup" /></p>
          </div>
        </form>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
 
  
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/form-validate.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
