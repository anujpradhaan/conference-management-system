<%-- 
    Document   : beforeloginheader
    Created on : Apr 14, 2014, 1:59:13 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header>
  <div id="logo">
    <div id="logo_text">
      <!-- class="logo_six", allows you to change the colour of the text -->
      <h1><a href="Login.jsp">BITS<span class="logo_six">-Connectro</span></a></h1>
      <h2>A Conference Management System</h2>
    </div>
  </div>
  <nav>
    <div id="menu_container">
      <ul class="sf-menu" id="nav">
        <li><a href="login.jsp">Login</a></li>
        <li><a href="signup.jsp">Sign Up</a></li>
        <li><a href="contact.jsp">Contact Us</a></li>
      </ul>
    </div>
  </nav>
</header>

