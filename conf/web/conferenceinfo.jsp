<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
      <%@include file="chairheader.jsp" %>
    <div id="site_content">
      <%@include file="chaircommonsidecontent.jsp"%>
            <div class="content">
          <%
model.Conference conference=(model.Conference)request.getAttribute("reviewerinfo");
%>
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" /><h1 style="margin: 15px 0 0 0;">Call For Papers</h1>
        <p align="justify">
            
             <%out.println(conference.getHeaderContent());%>
        </p>
        <p align="justify"><strong>This conference welcomes papers address on, but not limited to, the following research topics:</strong></p>
        <p><ul>
            <%
            String[] topics=conference.getTopics().split(",");
            for(int i=0;i<topics.length;i++){            
%>
            
            <li><%out.println(topics[i]);%></li>
<%
}
%>            
        </ul></p>
        <p align="justify"><Strong>Sponsers Info:</Strong>
        <%
        out.println(conference.getSponsersInfo());
%>
        </p>
        <p>
            <strong>Selected papers after modification will be published in the following reviewed journals.</strong>
        <ul>
            <%
String[] journals=conference.getJournals().split(",");
//if(journals.length==1)
  //  journals=conference.getJournals().split("\n");
for(int i=0;i<journals.length;i++){            
%>
            
            <li><%out.println(journals[i]);%></li>
<%
}
%>
        </ul>
        </p>
        <p>
            <strong>Important Dates:</strong>
        <table>
           
            <tr>
                <td>Starting Date</td><td><%
                out.println(conference.getStart_date());
%></td>
            </tr>
            <tr>
                <td>Ending Date</td><td><%
                out.println(conference.getEnd_date());
%></td>
            </tr>
            <tr>
                <td>Abstract Deadline</td><td><%
                out.println(conference.getAbstractDeadline());
%></td>
            </tr>
            <tr>
                <td>Final Paper Deadline</td><td><%
                                out.println(conference.getPaperDeadLine());
%></td>
            </tr>
        </table>
        </p>
        <p>
            <strong>Our Sponsers:</strong>
        <ul>
            <%
String[] sponsers=conference.getSponsers().split(",");
//if(journals.length==1)
  //  journals=conference.getJournals().split("\n");
for(int i=0;i<sponsers.length;i++){            
%>
            
            <li><%out.println(sponsers[i]);%></li>
<%
}
%>
        </ul>
        </p>
      </div>
    </div>
<%@include  file="footer.jsp"%>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/common.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
