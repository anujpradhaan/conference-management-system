<%-- 
    Document   : testtransaction
    Created on : Apr 18, 2014, 1:29:05 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form action="https://secure-test.worldpay.com/wcc/purchase" method=POST>
            <input type=hidden name="testMode" value="100" />
            <input type=hidden name="name" value="AUTHORISED">
            <input type="submit" />
        </form> 
    </body>
</html>
