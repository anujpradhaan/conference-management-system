<%-- 
    Document   : login
    Created on : Apr 14, 2014, 2:01:02 AM
    Author     : Dell
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BITS-Connectro</title>
          <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
          <script type="text/javascript" src="js/jquery-ui.js"></script>
          <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
          <link rel="stylesheet" href="css/jquery-ui.css"></link>
          <link rel="stylesheet" href="css/style.css"></link>
          <script src="js/common.js"></script>
          <script type="text/javascript">
              
            $(document).ready(function(){
               
               $("#loading").hide();
               $("#submitLoginForm").click(function(){
                  $("#loading").show();
                 alert("clicked");
                  var dataString=$("#loginForm").serialize();
                  $.ajax({
                    url:'Uconfctrl',
                    type:'post',
                    data:dataString,
                    dataType:'json',
                    beforeSend:function(){
                      
                    //	$("#loading").show();
                    },
                    success:function(data){
                        
                     
                        $("#loading").hide();
                         if(data.success=="failed"){
                             $("#loginMsg").text("Update Failed!!. Please try again");
                         } else{
                            
                             $("#redirectForm").submit();
                         }
                        
                            //if(data.hasOwnProperty('message'))
                            //{
                            //	var info=data.message;
                                    //alert($.trim(info[0]['info']));
                            /*	if(data.hasOwnProperty('menge'))
                                    {
                                            var infoData=titleInfoMessage(data.message,data.menge,title);
                                            showInfoDialog(title,infoData);	
                                    }
                                    else
                                    {
                                            showInfoDialog(title,info[0]['info']);
                                    }	*/
                            //}
                    },
                    error:function(){
                        alert("error");
                        
                    //	showError(__('Some_Error_Occured'));
                    }
                  });
               });
              $("#dialog-form").dialog({
              modal: true, 
              zIndex: 10000,
              title: "Login",
              show: { effect: "fadeIn", duration: 1200 },
              width: 'auto'
              });
          });
          </script>
    </head>
    <body>
    <div id="loading"><!--//added on 20th feb 2013-->
    <img id="loading-image" src="images/ajax-loader.gif" alt="Loading..." />
	<h3 id="loadingMsg">Updating..</h3>
    </div>    
  <div id="dialog-form" title="Create new user">
      <form action="authupdate" method="post" id="redirectForm">
          
      </form>
      <p class="validateTips">Conference Details:</p><span><p id="loginMsg"></p></span>
       <% out.println(request.getParameter("attb3").toString());
       String a=request.getParameter("attb3").toString(); %>
     
      <form action="#" method="post" id="loginForm">
          <div class="form_settings">
              <p><span>Conference Name</span><input type="text" name="cname" value="<%out.println(request.getParameter("attb1")); %>" placeholder="Conference name" class=""/><span class="error-message"></span></p>
            <p><span>Organizing Department</span><input type="text" name="dname" value="<%out.println(request.getParameter("attb2")); %>" placeholder="Department name" class=""/></p>
            <p><span>Organizing Company</span><input type="text" name="compname" value="<%out.println(request.getParameter("attb5")); %>" placeholder="Company name" class=""/></p>
            <p><span>Start Date</span><input type="date" name="sdate" value="<% out.println(request.getParameter("attb3").toString()); %> " class=""/></p>
            <p><span>End Date</span><input type="date" name="edate" value="<% out.println(a);%>" class=""/></p>
           <p><span>Description</span><textarea rows="8" cols="50" name="desc" value="hdbshd  gg"><%out.println(request.getParameter("attb6")); %></textarea></p>
           <input type="hidden" name="cid" id="cid" value=" <% out.println(request.getParameter("conf_id"));%>" />
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitLoginForm" name="name" value="Update" /></p>
          </div><!--
          <fieldset>
            <label for="email">Email</label>
            <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all">
          </fieldset>-->
      </form>
  </div>
  
  <div id="main">
    <%@include  file="header.jsp"%>
    <div id="site_content">
        <%@include  file="chaircommonsidecontent.jsp"%>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      
    });
  </script>
    </body>
</html>
