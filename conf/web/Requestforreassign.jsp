<%-- 
    Document   : login
    Created on : Apr 14, 2014, 2:01:02 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bits-Connectro</title>
          <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
          <script type="text/javascript" src="js/jquery-ui.js"></script>
          <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
          <link rel="stylesheet" href="css/jquery-ui.css"></link>
          <link rel="stylesheet" href="css/style.css"></link>
          <script type="text/javascript">
            $(document).ready(function(){
               $("#loading").hide();
               $("#submitLoginForm").click(function(){
                  $("#loading").show();
                  var dataString=$("#loginForm").serialize();
                  $.ajax({
                    url:'login',
                    type:'post',
                    data:dataString,
                    dataType:'json',
                    beforeSend:function(){
                    //	$("#loading").show();
                    },
                    success:function(data){
                        $("#loading").hide();
                         if(data.success=="failed"){
                             $("#loginMsg").text("Authentication Failed!!. Please try again");
                         } else{
                             $("#redirectForm").submit();
                         }
                        
                            //if(data.hasOwnProperty('message'))
                            //{
                            //	var info=data.message;
                                    //alert($.trim(info[0]['info']));
                            /*	if(data.hasOwnProperty('menge'))
                                    {
                                            var infoData=titleInfoMessage(data.message,data.menge,title);
                                            showInfoDialog(title,infoData);	
                                    }
                                    else
                                    {
                                            showInfoDialog(title,info[0]['info']);
                                    }	*/
                            //}
                    },
                    error:function(){
                    //	showError(__('Some_Error_Occured'));
                    }
                  });
               });
              $("#dialog-form").dialog({
              modal: true, 
              zIndex: 10000,
              title: "Login",
              show: { effect: "fadeIn", duration: 1200 },
              width: 'auto'
              });
          });
          </script>
    </head>
    <body>
    <%@include  file="loading.jsp"%>    
  <div id="dialog-form" title="Create new user">
      <form action="auth" method="post" id="redirectForm">
          
      </form>
      <p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>
      <form action="#" method="post" id="loginForm">
          <div class="form_settings">
            <p><span>Email :</span><input type="text" name="login" value="" /></p>
            <p><span>Password</span><input type="password" name="login" value="" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitLoginForm" name="name" value="Login" /></p>
          </div><!--
          <fieldset>
            <label for="email">Email</label>
            <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all">
          </fieldset>-->
      </form>
  </div>
  
  <div id="main">
    <%@include  file="header.jsp"%>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="images/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Latest News</h3>
          <h4>New Website Launched</h4>
          <h5>January 1st, 2012</h5>
          <p>2012 sees the redesign of our website. Take a look around and let us know what you think.<br /><a href="#">Read more</a></p>
        </div>
        <img class="paperclip" src="images/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Useful Links</h3>
          <ul>
            <li><a href="#">First Link</a></li>
            <li><a href="#">Another Link</a></li>
            <li><a href="#">And Another</a></li>
            <li><a href="#">Last One</a></li>
          </ul>
        </div>
      </div>
      <div class="content">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" /><h1 style="margin: 15px 0 0 0;">Welcome to the CSS3_six template</h1>
        <p>This simple, fixed width website template is released under a <a href="http://creativecommons.org/licenses/by/3.0">Creative Commons Attribution 3.0 Licence</a>. This means you are free to download and use it for personal and commercial projects. However, you <strong>must leave the 'design from css3templates.co.uk' link in the footer of the template</strong>.</p>
        <p>This template is written entirely in <strong>HTML5</strong> and <strong>CSS3</strong>.</p>
        <p>You can view more free CSS3 web templates <a href="http://www.css3templates.co.uk">here</a>.</p>
        <p>This template is a fully documented 5 page website, with an <a href="examples.html">examples</a> page that gives examples of all the styles available with this design. There is also a working PHP contact form on the contact page.</p>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      
    });
  </script>
    </body>
</html>
