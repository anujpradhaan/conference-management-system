<%-- 
    Document   : chairdashboard.jsp
    Created on : Apr 16, 2014, 11:35:39 AM
    Author     : Dell
--%>
<!DOCTYPE HTML>
<html>

<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/common.js"></script>
  <script type="text/javascript" src="js/chair.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
</head>

<body>
    <%@include  file="loading.jsp"%>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
      <%@include  file="chaircommonsidecontent.jsp"%>
      <div class="content">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" />
        <h1 style="margin: 15px 0 0 0;">Welcome to Chair Dashboard</h1>
        <p>This dashboard will provide all the admin/chair operations in utmost easy way. Below Present the details of all the
           conferences. 
        </p>
        <div id="tabs">
          <ul>
            <li><a href="#tabs-1">Abstracts</a></li>
            <li><a href="#tabs-2">Papers</a></li>
          </ul>
          <div id="tabs-1">
              <%
            java.util.List<model.PaperInfo> list=(java.util.List<model.PaperInfo>)request.getAttribute("paperinfo");
            //out.println(list);
            
%>          
            <p><strong>Abstract related Information:</strong></p>
            <div id="accordion">
              <h3>Recents Papers (Which are not assigned to anyone)</h3>
              <div>
                
                    <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Assign</th><th>Reject</th></tr>
                      <%
                      java.util.Iterator<model.PaperInfo> itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("abstract")&& paperInfoObject.getPaperStatus()==0){

%>
                      <tr>
                          <td><%out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><%out.println(paperInfoObject.getAuthorName());%></td>
                          <td><input type="button" class="submit assignToReviewer" value="Assign Reviewer"  data-PaperId="<%out.println(paperInfoObject.getPaperId());%>" /></td>
                          <td><input type="button" class="submit rejectPaper" value="Reject"  data-PaperId="<%out.println(paperInfoObject.getPaperId());%>"/></td>
                      </tr>
                      <%
    }
}
                                                                    
%>
                    </table>
                
              </div>
                    
              <h3>Pending Papers(Not approved by any reviewer)</h3>
              <div>
                    <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Reject</th></tr>
                      <%
                      
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("abstract") && paperInfoObject.getPaperStatus()==1){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><input type="button" class="submit rejectPaper" value="Reject" data-PaperId="<%out.println(paperInfoObject.getPaperId());%>"/></td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>
                    
                    
              <h3>Approved papers(Yet to be evaluated by reviewer)   </h3>
              <div>
                <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Marks</th><th>Reject</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("abstract") && paperInfoObject.getPaperStatus()==2){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><% out.println(paperInfoObject.getMarks());%></td>
                          <td><input type="button" class="submit rejectPaper" value="Reject" data-PaperId="<%out.println(paperInfoObject.getPaperId());%>"/></td>
                          <td><input type="button" class="submit acceptPaper" value="Accept" data-PaperId="<%out.println(paperInfoObject.getPaperId());%>"/></td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>
              <h3>Rejected Papers (Which are rejected before)</h3>
              <div>
                    <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Marks</th><th>Status</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("abstract") && paperInfoObject.getPaperStatus()==4){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><% out.println(paperInfoObject.getMarks());%></td>
                          <td>Rejected</td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
                  
                  
              </div>
              <h3>Approved Papers</h3>
              <div>
                    <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Marks</th><th>Status</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("abstract") && paperInfoObject.getPaperStatus()==3){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><% out.println(paperInfoObject.getMarks());%></td>
                          <td>Approved</td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
                  
                  
              </div>      
            </div>
          </div> 
          <div id="tabs-2">
          <p><strong>Final Paper Related Information:</strong></p>
           <div id="accordion1">
              <h3>Recents Papers (Which are not assigned to anyone)</h3>
              <div>
                <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Assign</th><th>Reject</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("finalpaper") && paperInfoObject.getPaperStatus()==0){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><input type="button" class="submit assignToReviewer" value="Assign Reviewer" data-PaperId="<% out.println(paperInfoObject.getPaperId());%>" /></td>
                          <td><input type="button" class="submit rejectPaper" value="Reject" data-PaperId="<% out.println(paperInfoObject.getPaperId());%>"/></td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>
              <h3>Pending Papers (Which are not approved by any reviewer)</h3>
              <div>
                <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Assign</th><th>Reject</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("finalpaper") && paperInfoObject.getPaperStatus()==1){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><input type="button" class="submit rejectPaper" value="Reject" data-PaperId="<% out.println(paperInfoObject.getPaperId());%>"/></td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>
              <h3>Approved papers(Yet to be evaluated by Reviewer)</h3>
              <div>
                <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Assign</th><th>Reject</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("finalpaper") && paperInfoObject.getPaperStatus()==2){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><input type="button" class="submit acceptPaper" value="Accept" data-PaperId="<% out.println(paperInfoObject.getPaperId());%>" /></td>
                          <td><input type="button" class="submit rejectPaper" value="Reject" data-PaperId="<% out.println(paperInfoObject.getPaperId());%>"/></td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>
              <h3>Rejected papers</h3>
              <div>
                <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Assign</th><th>Reject</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("finalpaper") && paperInfoObject.getPaperStatus()==4){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><% out.println(paperInfoObject.getMarks());%> /></td>
                          <td>Rejected</td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>      
              <h3>Approved papers</h3>
              <div>
                <table style="width:100%; border-spacing:0;">
                      <tr><th>Title</th><th>Author</th><th>Assign</th><th>Reject</th></tr>
                      <%
                      itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          if(paperInfoObject.getTypeOfPaper().equals("finalpaper") && paperInfoObject.getPaperStatus()==3){

%>
                      <tr>
                          <td><% out.println(paperInfoObject.getPaperTitle());%></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><% out.println(paperInfoObject.getMarks());%> /></td>
                          <td>Approved</td>
                      </tr>
                      <%
    }
}                                               
%>
                    </table>
              </div>      
            </div>
        </div> 
        </div>
       </div>
    </div>
    <div id="allAuthor-form" title="Create new user">
      <!--<p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>-->
      <form action="#" method="post" id="authorForm">
          <div class="form_settings">
            <p><span>Subject :</span><input type="text" name="subject" id="authorSubject" value="" /></p>
            <p><span>Message :</span><textarea rows="15" id="authorEmailBody"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitAllAuthorForm" name="name" value="Send Email" /></p>
          </div>
      </form>
    </div>
    <div id="allReviewer-form" title="Create new user">
      <!--<p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>-->
      <form action="#" method="post" id="reviewerForm">
          <div class="form_settings">
            <p><span>Subject :</span><input type="text" id="reviewerSubject" name="login" value="" /></p>
            <p><span>Message :</span><textarea rows="15" id="reviewerEmailBody"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitAllReviewerForm" name="name" value="Send Email" /></p>
          </div>
      </form>
    </div>
    <div id="anyIndividual-form" title="Create new user">
      <!--<p class="validateTips">Login:</p><span><p id="loginMsg"></p></span>-->
      <form action="#" method="post" id="individualForm">
          <div class="form_settings">
            <p><span>Email :</span><input type="email" name="login" id="anyEmail" value="" /></p>  
            <p><span>Subject :</span><input type="text" id="anySubject" name="login" value="" /></p>
            <p><span>Message :</span><textarea rows="15" id="anyEmailBody"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="button" id="submitIndividualForm" name="name" value="Send Email" /></p>
          </div>
      </form>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  
  
  
</body>

</html>

