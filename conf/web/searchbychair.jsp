<%-- 
    Document   : searchbychair
    Created on : Apr 18, 2014, 11:09:50 PM
    Author     : ATIT
--%>
<!DOCTYPE HTML>
<html>
<head>
  <title>BITS-Connectro</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/common.js"></script>
</head>

<body>
  <div id="main">
    <%@include  file="chairheader.jsp"%>
    <div id="site_content">
        <div class="content" style="width:100%">
        <img style="float: left; vertical-align: middle; margin: 0 10px 0 0;" src="images/home.png" alt="home" />
        <h2>Search Using:</h2>
        <form action="ChairSearch" method="post">
          <div class="form_settings">
            <p>
                <select id="id" name="searchtype" id="searchtype" style="width: 145px;">
                    <option value="1">Title</option>
                    <option value="2">Author Name</option>
                    <option value="3">Reviewer Name</option>
                    <option value="4">Conference Name</option>
                    <option value="5">Payment</option>
                </select>
                <input type="text" name="searchvalue" style="width: 170px;"/>
                <input class="submit" style="margin: 0 0 0 11px;" type="submit" name="name" value="Search" />
            </p>
          </div>
        </form>
        <table style="width:100%; border-spacing:0;">
            <tr><th>Paper Title</th><th>Author Name</th><th>Assigned Reviewer</th><th>Conference Name</th></tr>
       <%
            java.util.List<model.PaperInfo> list=(java.util.List<model.PaperInfo>)request.getAttribute("paperinfo");
%>
    <%
                      java.util.Iterator<model.PaperInfo> itr=list.iterator();
                      while(itr.hasNext()){
                          model.PaperInfo paperInfoObject=itr.next();
                          

%>
                      <tr>
                          <td><a href="<%out.println("data/"+paperInfoObject.getUserId()+"/"+paperInfoObject.getFileName());%>" target="_new"><% out.println(paperInfoObject.getPaperTitle());%></a></td>
                          <td><% out.println(paperInfoObject.getAuthorName());%></td>
                          <td><% out.println(paperInfoObject.getReviewerName());%></td>
                          <td><a href="<%out.println("viewConference?id="+paperInfoObject.getConferenceId());%>" target="_new"><% out.println(paperInfoObject.getConferenceName());%></a></td>
                      </tr>
                      <%
    //}
}                                               
%>
            
            
          </table>
      </div>
    </div>
    <%@include file="footer.jsp" %>  
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      
    });
  </script>
  
</body>

</html>

